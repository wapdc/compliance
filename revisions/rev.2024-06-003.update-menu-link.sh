set -e
$PSQL_CMD wapdc <<EOF
INSERT INTO public.menu_links (category, menu, title, abstract, icon, action, url, permission, count_function)
VALUES ('Compliance', 'staff', 'Reporting Periods Management', 'Find information on reporting period dates. Edit dates, generate new reporting dates.', '', 'Search', '/compliance/#/reporting/periods/edit/C4', 'administer wapdc data', null);
EOF