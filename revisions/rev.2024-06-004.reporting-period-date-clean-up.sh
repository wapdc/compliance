set -e
$PSQL_CMD wapdc <<EOF
Delete from reporting_period
where start_date > '2030-12-31'
and reporting_period_type = 'Monthly'
and report_type = 'C4';
EOF