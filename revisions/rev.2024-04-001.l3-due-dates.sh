set -e
$PSQL_CMD wapdc <<EOF
INSERT INTO reporting_period (reporting_period_type, election_date, start_date, end_date, due_date, report_type, reminder_date, reminder_sent_date) VALUES ('Annual', null, '2017-01-01', '2017-12-31', '2018-02-28', 'L3', null, null);
INSERT INTO reporting_period (reporting_period_type, election_date, start_date, end_date, due_date, report_type, reminder_date, reminder_sent_date) VALUES ('Annual', null, '2018-01-01', '2018-12-31', '2019-02-28', 'L3', null, null);
INSERT INTO reporting_period (reporting_period_type, election_date, start_date, end_date, due_date, report_type, reminder_date, reminder_sent_date) VALUES ('Annual', null, '2019-01-01', '2019-12-31', '2020-02-28', 'L3', null, null);
INSERT INTO reporting_period (reporting_period_type, election_date, start_date, end_date, due_date, report_type, reminder_date, reminder_sent_date) VALUES ('Annual', null, '2020-01-01', '2020-12-31', '2021-03-01', 'L3', null, null);
INSERT INTO reporting_period (reporting_period_type, election_date, start_date, end_date, due_date, report_type, reminder_date, reminder_sent_date) VALUES ('Annual', null, '2021-01-01', '2021-12-31', '2022-02-28', 'L3', null, null);
INSERT INTO reporting_period (reporting_period_type, election_date, start_date, end_date, due_date, report_type, reminder_date, reminder_sent_date) VALUES ('Annual', null, '2022-01-01', '2022-12-31', '2023-02-28', 'L3', null, null);
INSERT INTO reporting_period (reporting_period_type, election_date, start_date, end_date, due_date, report_type, reminder_date, reminder_sent_date) VALUES ('Annual', null, '2023-01-01', '2023-12-31', '2024-02-28', 'L3', null, null);
INSERT INTO reporting_period (reporting_period_type, election_date, start_date, end_date, due_date, report_type, reminder_date, reminder_sent_date) VALUES ('Annual', null, '2024-01-01', '2024-12-31', '2025-02-28', 'L3', null, null);
INSERT INTO reporting_period (reporting_period_type, election_date, start_date, end_date, due_date, report_type, reminder_date, reminder_sent_date) VALUES ('Annual', null, '2025-01-01', '2025-12-31', '2026-03-02', 'L3', null, null);
INSERT INTO reporting_period (reporting_period_type, election_date, start_date, end_date, due_date, report_type, reminder_date, reminder_sent_date) VALUES ('Annual', null, '2026-01-01', '2026-12-31', '2027-03-01', 'L3', null, null);
INSERT INTO reporting_period (reporting_period_type, election_date, start_date, end_date, due_date, report_type, reminder_date, reminder_sent_date) VALUES ('Annual', null, '2027-01-01', '2027-12-31', '2028-02-28', 'L3', null, null);
INSERT INTO reporting_period (reporting_period_type, election_date, start_date, end_date, due_date, report_type, reminder_date, reminder_sent_date) VALUES ('Annual', null, '2028-01-01', '2028-12-31', '2029-02-28', 'L3', null, null);
INSERT INTO reporting_period (reporting_period_type, election_date, start_date, end_date, due_date, report_type, reminder_date, reminder_sent_date) VALUES ('Annual', null, '2029-01-01', '2029-12-31', '2030-02-28', 'L3', null, null);
INSERT INTO reporting_period (reporting_period_type, election_date, start_date, end_date, due_date, report_type, reminder_date, reminder_sent_date) VALUES ('Annual', null, '2030-01-01', '2030-12-31', '2031-02-28', 'L3', null, null);
INSERT INTO reporting_period (reporting_period_type, election_date, start_date, end_date, due_date, report_type, reminder_date, reminder_sent_date) VALUES ('Annual', null, '2031-01-01', '2031-12-31', '2032-03-01', 'L3', null, null);
INSERT INTO reporting_period (reporting_period_type, election_date, start_date, end_date, due_date, report_type, reminder_date, reminder_sent_date) VALUES ('Annual', null, '2032-01-01', '2032-12-31', '2033-02-28', 'L3', null, null);


EOF