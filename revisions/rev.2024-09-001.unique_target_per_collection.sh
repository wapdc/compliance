set -e
$PSQL_CMD wapdc <<EOF
create unique index collection_member_collection_id_target_id_uindex
    on public.collection_member (collection_id, target_id);
EOF
