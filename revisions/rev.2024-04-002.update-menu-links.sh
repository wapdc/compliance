set -e
$PSQL_CMD wapdc <<EOF
UPDATE public.menu_links
SET url = '/compliance/#/collections/candidacy'
WHERE link_id = 40;

UPDATE public.menu_links
SET url = '/compliance/#/collections/officials'
WHERE link_id = 42;

UPDATE public.menu_links
SET url = '/compliance/#/collections/jurisdiction'
WHERE link_id = 46;

UPDATE public.menu_links
SET url = '/compliance/#/collections/person'
WHERE link_id = 41;

INSERT INTO public.menu_links (link_id, category, menu, title, abstract, icon, action, url, permission, count_function)
VALUES (DEFAULT, 'Other', 'staff', 'Lobbyist client research',
        'Lobbyist client research and outreach collections for lobbyist clients that support business processes such as email notifications and group enforcement (L3)',
        null, null, '/compliance/#/collections/lobbyist_client', 'access wapdc data', null);
EOF
