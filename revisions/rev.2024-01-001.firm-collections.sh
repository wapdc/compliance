set -e
$PSQL_CMD wapdc <<EOF
insert into menu_links(category, menu, title, abstract,  url, permission)
  values (
          'Other',
          'staff',
          'Lobbyist firm research',
          'Lobbyist firm research and outreach collections for lobbyist firms that support business processes such as email notifications and group enforcement (L2)',
           '/compliance/#/collections/lobbyist_firm',
          'access wapdc data');
EOF
