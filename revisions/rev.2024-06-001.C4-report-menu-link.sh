set -e
$PSQL_CMD wapdc <<EOF
insert into menu_links(category, menu, title, abstract,  url, permission)
  values (
          'Compliance',
          'staff',
          'C4 Report Compliance',
          'Display the compliance rates for candidates filing their C4 reports for both primary/general elections for all years.',
           '/compliance/#/reports/C4-report-compliance',
          'access wapdc data');
EOF