select lf.lobbyist_firm_id,
       lf.name,
       lf.email,
       trim (COALESCE(lf.address_1,'') || ' ' || coalesce(lf.address_2,'')) as address,
       lf.city,
       lf.state,
       lf.postcode,
       lf.phone,
       lf.updated_at
from lobbyist_firm lf
         join (select distinct lr.lobbyist_firm_id
                    from lobbyist_reporting_periods lr
                             inner join lobbyist_contract lcr on lr.lobbyist_contract_id = lcr.lobbyist_contract_id
                    where (:year::int is null or (extract(year from lr.period_start) = :year::int or extract(year from lr.period_end) = :year::int))
                      and (:has_subcontractor::text is null or
                           (:has_subcontractor::text = 'true' and lcr.contractor_id is not null) or
                           (:has_subcontractor::text = 'false' and lcr.contractor_id is null))) as filtered_lrp
                   on lf.lobbyist_firm_id = filtered_lrp.lobbyist_firm_id
         left join (select target_id
                    from pdc_user_authorization
                    where target_type = 'lobbyist_firm'
                    group by target_id) as pua on lf.lobbyist_firm_id = pua.target_id
where (:has_user_account::text is null or
       (:has_user_account::text = 'true' and pua.target_id is not null) or
       (:has_user_account::text = 'false' and pua.target_id is null))