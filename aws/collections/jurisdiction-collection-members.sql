SELECT cm.*,
    cm.label AS jurisdiction,
    coalesce(case when trim(jc.name) <> '' then jc.name end , j.name) as name, j.rpts,
    j.officials_confirmed_at,
    j.category as jurisdiction_category,
    ols.updated_at as ols_updated_at
FROM collection_member cm
         JOIN (select jcn.jurisdiction_id, string_agg(jcn.name, ',') as name from jurisdiction_contact jcn group by jcn.jurisdiction_id) jc ON cm.target_id = jc.jurisdiction_id
         JOIN jurisdiction j ON cm.target_id = j.jurisdiction_id
         LEFT JOIN officials_list_submission ols on ols.jurisdiction_id = j.jurisdiction_id
WHERE collection_id = :collection_id
ORDER BY label;