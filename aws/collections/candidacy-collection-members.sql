SELECT
    cm.member_id,
    excluded,
    cm.target_id,
    cm.compare_id,
    cm.email,
    cm.memo,
    cm.flag,
    ca.election_code,
    ca.candidacy_id,
    ca.office_code,
    ca.jurisdiction_id,
    ca.general_result,
    ca.primary_result,
    ca.exit_reason,
    case when (ca.sos_candidate_id is not null or sos_candidate_code is not null or ca.declared_date is not null) then 'Y' END AS declared,
    COALESCE(ca.filer_id, p.filer_id) as filer_id,
    j.name as jurisdiction,
    o.offtitle as office,
    e.title as election,
    j.county,
    j.rpts,
    COALESCE(ca.ballot_name, p.name) as ballot_name,
    p.person_id,
    p.name,
    cm.address,
    cm.city,
    cm.state,
    cm.postcode,
    cm.phone,
    p.sortcode,
    cct.tags,
    j.category as jurisdiction_category,
    cm.last_contacted,
    ca.campaign_start_date,
    cm.metadata
FROM collection_member cm
    JOIN candidacy ca ON cm.target_id = ca.candidacy_id
    JOIN person p ON ca.person_id = p.person_id
    JOIN jurisdiction j ON ca.jurisdiction_id = j.jurisdiction_id
    JOIN foffice o ON ca.office_code = o.offcode
    JOIN election e ON ca.election_code = e.election_code
    LEFT JOIN compliance_case cc on cc.tid =cast(cm.compare_id as varchar)
    LEFT JOIN (select array_agg(tag) as tags, ccid from compliance_case_tag group by ccid) cct on cct.ccid = cc.ccid
WHERE collection_id = :collection_id
ORDER BY e.election_code, name
