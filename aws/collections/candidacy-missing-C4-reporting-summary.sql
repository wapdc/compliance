select
    ca.candidacy_id,
    p.name as name,
    ca.election_code,
    ca.declared_email as email,
    ca.address,
    ca.city,
    ca.state,
    ca.postcode,
    ca.exit_reason,
    jsonb_build_object('reports', jsonb_agg( jsonb_build_object(
                    'reporting_period_type', report_summary.reporting_period_type,
                    'reporting_period_id', report_summary.reporting_period_id,
                    'period_start', report_summary.start_date,
                    'period_end', report_summary.end_date,
                    'is_filed', report_summary.is_filed,
                    'reporting_type', report_summary.reporting_type,
                    'first_submitted_at', report_summary.first_submitted_at,
                    'last_submitted_at', report_summary.last_submitted_at
                     )
                                  order by report_summary.start_date
                    )
    ) as metadata
from candidacy ca
join ( select
            ca.candidacy_id,
            rp.reporting_period_id,
            rp.reporting_period_type,
            rp.start_date,
            rp.end_date,
            min(r.report_id) as report_id,
            min(r.submitted_at) as first_submitted_at,
            max(r.submitted_at) as last_submitted_at,
            case when ((min(r.period_start) is null) or min(r.period_start) > (greatest(min(rp.start_date), min(ca.campaign_start_date)))) or
                      ((max(r.period_end) is null) or max(r.period_end) < max(rp.end_date))
                     then false else true end as is_filed,
            case when c.committee_id is null and j.rpts = 'C' then 'full' when c.committee_id is null then null else cf_reporting_status(c.committee_id, rp.start_date) end as reporting_type,
            rp.due_date,min(r.period_start) as period_start,max(r.period_end) as period_end
        from candidacy ca
                join election e on ca.election_code = e.election_code
                join reporting_period rp on rp.reporting_period_type in ('PostGeneral', 'SevenDayPreGeneral', 'TwentyOneDayPreGeneral') and rp.report_type = 'C4' and e.election_date = rp.election_date and extract(year from rp.start_date) = cast(:year as int)
                join jurisdiction j on ca.jurisdiction_id = j.jurisdiction_id
                left join committee c on ca.committee_id = c.committee_id
                left join fund f on c.committee_id = f.committee_id and f.election_code > '2018' and f.election_code = e.election_code
                left join report r on f.fund_id = r.fund_id and ((rp.start_date between r.period_start and r.period_end) or (rp.end_date between r.period_start and r.period_end))
            and r.superseded_id is null
        where
            ((c.committee_id is null and j.rpts = 'C') or (c.committee_id is not null and cf_reporting_status(c.committee_id, rp.start_date) = 'full'))
          and ca.exit_reason is null
          and ca.declared_date is not null
          and ca.in_general
        group by ca.candidacy_id, rp.reporting_period_id, rp.reporting_period_type, rp.start_date, rp.end_date, rp.due_date, c.committee_id, j.rpts
        ) as report_summary on report_summary.candidacy_id = ca.candidacy_id
        join person p on ca.person_id = p.person_id
group by ca.candidacy_id,p.name,ca.election_code,ca.declared_email,ca.address,ca.city, ca.state, ca.postcode, ca.exit_reason
having ((cast(:is_not_filed as bool) = true and bool_and(is_filed) <> true)
    or (cast(:is_submitted_late as bool) = true and bool_or(report_summary.first_submitted_at >= report_summary.due_date))
        or (cast(:is_amended_late as bool) = true and bool_or(report_summary.last_submitted_at >= report_summary.due_date))
    )
