select
    f.name,
    trim (COALESCE(f.address_1,'') || ' ' || coalesce(f.address_2,'')) as address,
    f.city,
    f.state,
    f.postcode,
    case when f.email != e.email then concat(f.email, ';', e.email) else f.email end as email,
    f.phone,
    f.lobbyist_firm_id,
    r.metadata
from (select lr.lobbyist_firm_id,
       json_build_object(
                        'name', max(f.name),
                        'reports', json_agg(json_build_object(
                        'm', extract(month from lr.period_start ),
                        'period', to_char(lr.period_start, 'Month YYYY'),
                        'period_start', lr.period_start,
                        'status', case when l2.report_id is not null then 'filed' else 'missing' end,
                        'is_filed', case when l2.report_id is not null then true else false end,
                        'first_submitted_at', l2s.first_submitted,
                        'last_submitted_at', l2s.last_submitted,
                        'is_submitted_late', case when (l2s.first_submitted > rp.due_date) then true end,
                        'is_amended_late', case when (l2s.last_submitted > rp.due_date) then true end
                ) order by lr.period_start)) as metadata
  from
    (select lrp.lobbyist_firm_id, lrp.period_start, lrp.period_end
     from lobbyist_reporting_periods lrp group by lrp.lobbyist_firm_id, lrp.period_start, lrp.period_end
                                         having bool_and(lrp.exempt) = false) lr
  left join reporting_period rp on rp.start_date = lr.period_start and rp.report_type = 'L2'
  left join l2 on l2.lobbyist_firm_id=lr.lobbyist_firm_id and l2.period_start=lr.period_start
  left join lobbyist_firm f on f.lobbyist_firm_id = lr.lobbyist_firm_id
  left join (select report_id, min(submitted_at) first_submitted, max(submitted_at) last_submitted from l2_submission s group by report_id) l2s on l2s.report_id=l2.report_id
  where lr.period_start between cast(:year || '-01-01' as date) and cast(:year || '-12-31' as date)
    and lr.period_end<= now()
    and extract(month  from lr.period_start) between cast(:start_month as int) and cast(:end_month as int)
  group by lr.lobbyist_firm_id
  having (
         (Coalesce(cast(:is_not_filed as bool), false) and (count(lr.lobbyist_firm_id) <> count(l2.report_id)))
         and (not coalesce(cast(:is_submitted_late as bool), false) or (bool_or((l2s.first_submitted > rp.due_date))))
         and (not coalesce(cast(:is_amended_late as bool), false ) or (bool_or((l2s.last_submitted > rp.due_date))))
     )
  ) r
join lobbyist_firm f on f.lobbyist_firm_id=r.lobbyist_firm_id
join entity e on e.entity_id = f.entity_id
