select lc.lobbyist_client_id,
       lc.name,
       lc.email,
       trim(coalesce(lc.address_1, '') || ' ' || coalesce(lc.address_2, '')) as address,
       lc.city,
       lc.state,
       lc.postcode,
       lc.phone,
       lc.updated_at
from lobbyist_client lc
         join (select distinct lr.lobbyist_client_id
                    from lobbyist_reporting_periods lr
                            join lobbyist_contract lcr on lr.lobbyist_contract_id = lcr.lobbyist_contract_id
                    where (:year::int is null or (extract(year from lr.period_start) = :year::int or extract(year from lr.period_end) = :year::int))
                      and (:has_subcontractor::text is null or
                           (:has_subcontractor::text = 'true' and lcr.contractor_id is not null) or
                           (:has_subcontractor::text = 'false' and lcr.contractor_id is null))) as filtered_lrp
                   on lc.lobbyist_client_id = filtered_lrp.lobbyist_client_id
         left join (select target_id
                    from pdc_user_authorization
                    where target_type = 'lobbyist_client'
                    group by target_id) as pua on lc.lobbyist_client_id = pua.target_id
where (:has_user_account::text is null or
       (:has_user_account::text = 'true' and pua.target_id is not null) or
       (:has_user_account::text = 'false' and pua.target_id is null))