select
  cm.member_id,
  cm.label,
  o.official_id,
  CASE
    WHEN o.official_id is null then true
    WHEN o.end_date < make_date(:year, 1, 1) then true
    WHEN p.inactive_date is not null then true
    ELSE false END ended
from collection_member cm
  left join official o on cm.target_id = o.official_id
  left join person p on p.person_id = o.person_id
where cm.collection_id = :collection_id
