SELECT cm.member_id,
       excluded,
       cm.target_id,
       cm.email,
       cm.memo,
       cm.flag,
       cm.compare_id,
       p.name,
       p.person_id,
       j.jurisdiction_id,
       j.county,
       j.rpts,
       j.name as jurisdiction,
       j.category as jurisdiction_category,
       o.end_date,
       fo.offtitle as office,
       cm.address,
       cm.city,
       cm.state,
       cm.postcode,
       cm.phone,
       cm.last_contacted
FROM collection_member cm
  JOIN official o ON cm.target_id = o.official_id
  JOIN person p ON o.person_id = p.person_id
  JOIN jurisdiction j ON o.jurisdiction_id = j.jurisdiction_id
  JOIN foffice fo ON o.office_code = fo.offcode
  LEFT JOIN compliance_case cc on cc.tid =cast(cm.compare_id as varchar)
  LEFT JOIN (select array_agg(tag) as tags, ccid from compliance_case_tag group by ccid) cct on cct.ccid = cc.ccid
  WHERE collection_id = :collection_id
  ORDER BY p.name