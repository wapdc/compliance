select j.*,
       jc.email,
       jca.address,
       jca.premise,
       jca.city,
       jca.state,
       jca.postcode,
       jca.phone
from jurisdiction j
      join (select jcg.jurisdiction_id, string_agg(jcg.email, ';') as email  from jurisdiction_contact jcg
               group by jcg.jurisdiction_id) jc on j.jurisdiction_id = jc.jurisdiction_id
         left join
     (select jc2.*, row_number() over (partition by jc2.jurisdiction_id order by jc2.updated_at desc ) r
      from jurisdiction_contact jc2 where address is not null) jca
     on jca.jurisdiction_id=j.jurisdiction_id and r=1
where
    ((:reporting_category = 1 and j.rpts = 'C')
      or (:reporting_category = 2 and j.rpts in ('C','F'))
      or :reporting_category = 3)
    and (cast(:jurisdiction_categories as int[]) is null or j.category = any(cast(:jurisdiction_categories as int[])) )
    and j.inactive is null


