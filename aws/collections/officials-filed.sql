SELECT official_id,
       p.name,
       'filed' as filed,
       TRUE reports_filed
FROM official of JOIN person p ON of.person_id = p.person_id
                 JOIN fa_statement fs
                      ON  of.person_id = fs.person_id AND
                          (fs.period_end BETWEEN make_date(CAST(:year AS INTEGER), 12, 31) AND make_date(CAST(:year AS INTEGER) + 1, 11, 30)
                              OR (of.end_date between fs.period_start and fs.period_end AND fs.period_start <= make_date(cast(:year as integer),12,31)))
WHERE
        of.start_date <= make_date(CAST(:year AS integer) + 1,04,15)
