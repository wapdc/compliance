select
       ca.candidacy_id,
    ca.declared_email as email,
    ca.address,
    ca.city,
    ca.state,
    ca.postcode,
    ca.exit_reason,
    p.name
from candidacy ca
         join person p on ca.person_id = p.person_id
         join jurisdiction j on ca.jurisdiction_id = j.jurisdiction_id
         left join jurisdiction_category jc on j.category = jc.jurisdiction_category_id
where election_code = :election_code
  and ((cast(:has_exit_reason as boolean) is distinct from true) or exit_reason is not null)
  and ((cast(:is_declared as boolean) is distinct from true) or (sos_candidate_id is not null or sos_candidate_code is not null))
  and (cast(:has_position as boolean) is distinct from true or position_id is not null)
  and (cast(:jurisdiction_category as int) is null or   jc.jurisdiction_category_id =:jurisdiction_category)
  and (cast(:jurisdictions as int[]) is null or  ca.jurisdiction_id = any(cast( :jurisdictions as int[])))
  and (cast(:offices as text[]) is null or ca.office_code = any(cast(:offices as text[])))