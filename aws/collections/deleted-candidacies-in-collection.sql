select
  c.candidacy_id,
  cm.member_id,
  case
    when c.candidacy_id is null then true
    else false
  end deleted
from collection_member cm
  left join candidacy c ON cm.target_id = c.candidacy_id
where collection_id = cast(:collection_id as integer)
order by target_id;