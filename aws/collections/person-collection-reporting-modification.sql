select p.* from person p join
(select sub.person_id
from fa_submission sub
  join fa_statement st on st.submission_id = sub.submission_id
left join reporting_modification rm on sub.person_id = rm.target_id
    and target_type = 'person'
    and rm.decision_code = 'approved'
    and rm.modification_type = 'residential_address'
    and rm.start_date <= sub.period_end
    and coalesce(rm.end_date, sub.period_end) >= sub.period_end
where sub.verified = true
  and extract(year from st.period_start) = :year
  and rm.reporting_mod_id is null
  and cast((cast(sub.user_data as json) -> 'statement' ->> 'judge_or_law_enforcement') as boolean) = false
  and cast((cast(sub.user_data as json) -> 'statement' -> 'reporting_modifications' ->> 'residential_address') as boolean) = true
  group by sub.person_id) fs
on fs.person_id = p.person_id
