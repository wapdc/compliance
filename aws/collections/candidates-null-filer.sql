select c.*,
       c.declared_email as email,
       p.phone,
       p.name
from candidacy c
         join person p on c.person_id = p.person_id
where c.filer_id is null
  and election_code = :election_code
  and exit_reason in ('deceased', 'withdrew') is not true
