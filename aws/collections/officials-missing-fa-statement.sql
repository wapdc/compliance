SELECT
  p.name,
  CASE
    WHEN lower(of.email) <> lower(p.email) THEN CONCAT(of.email, ';', p.email)
    ELSE COALESCE(of.email, p.email) END AS email,
  CASE
    WHEN of.address IS NOT NULL THEN of.address
    ELSE p.address END AS address,
  CASE
    WHEN of.address IS NOT NULL THEN of.city
    ELSE p.city END AS city,
  CASE
    WHEN of.address IS NOT NULL THEN of.postcode
    ELSE p.postcode END AS postcode,
  of.state,
  p.phone,
  of.official_id,
  CASE
    WHEN fs.statement_id IS NOT NULL THEN 'filed'
    ELSE 'missing report' END filed
from official of
  JOIN person p ON of.person_id = p.person_id
  JOIN jurisdiction j ON of.jurisdiction_id = j.jurisdiction_id
  JOIN jurisdiction_category jc ON j.category = jc.jurisdiction_category_id
  left join jurisdiction_status js
    on js.jurisdiction_id = j.jurisdiction_id
      -- general election = tuesday after first monday = NOV_2 + ((7 + 2 - NOV_2::dow) % 7)
      and make_date(:year, 11, 2) + (9 - extract(dow from make_date(:year, 11, 2))::integer % 7)
        between js.valid_from and coalesce(js.valid_to, current_date)
  LEFT JOIN fa_statement fs
    on fs.person_id = p.person_id AND (fs.period_end BETWEEN make_date(CAST(:year AS INTEGER), 12, 31) AND make_date(CAST(:year AS INTEGER) + 1, 11, 30)
                              OR (of.end_date between fs.period_start and fs.period_end AND fs.period_start <= make_date(cast(:year as integer),12,31)))
where fs.statement_id is null
  and of.start_date <= make_date(cast(:year as integer) + 1,04,01)
  and (of.end_date is null or of.end_date >= make_date(cast(:year as integer),2,14))
  and coalesce(js.status, j.rpts) <> 'N'
  and (cast(:jurisdiction_categories as int[]) is null or (jc.jurisdiction_category_id = any(cast(:jurisdiction_categories as int[]))))
  and p.inactive_date is null
  -- jurisdiction was valid last year
  and coalesce(j.valid_to, current_date) >= date_trunc('year', current_date - interval '1 year')
  and j.inactive is null
