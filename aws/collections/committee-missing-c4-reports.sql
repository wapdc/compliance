SELECT c.name,
       c.start_year,
       c.end_year,
       c.election_code,
       c.committee_id     AS committee_id,
       cc.contact_emails AS email,
       cc2.address       AS address,
       cc2.city          AS city,
       cc2.state         AS state,
       cc2.postcode      AS postcode,
       cc2.phone         AS phone
FROM (SELECT c.*,
             Min(r.period_start),
             Max(r.period_end)
      FROM committee c
               JOIN committee_relation cr ON c.committee_id = cr.committee_id
               JOIN proposal pr ON cr.target_id = pr.proposal_id
               JOIN reporting_period rp ON rp.report_type = 'C4'
          AND rp.reporting_period_id = :reporting_period_id
               LEFT JOIN fund f ON c.committee_id = f.committee_id
               LEFT JOIN report r ON f.fund_id = r.fund_id and r.report_type = 'C4'
          AND ((rp.start_date BETWEEN r.period_start AND r.period_end) OR
               (rp.end_date BETWEEN r.period_start AND r.period_end))
      WHERE pr.election_code = Cast(:election_code AS VARCHAR)
        AND cf_reporting_status(c.committee_id, rp.start_date) = 'full'
        AND c.committee_type = 'CO'
        AND Cast(:election_code AS INT) BETWEEN c.start_year
          AND Coalesce( c.end_year, Cast(:election_code AS INT))
      GROUP BY c.committee_id
      HAVING ( Min(r.period_start) IS NULL OR Min(r.period_start) > Min(rp.start_date))
          OR (Max(r.period_end) IS NULL OR Max(r.period_end) < Max(rp.end_date))
     ) c
         LEFT JOIN
     (SELECT committee_id,
             Array_to_string(Array_agg(DISTINCT email), ';') AS contact_emails
      FROM committee_contact
      WHERE treasurer = TRUE
         OR ROLE = 'committee'
      GROUP BY committee_id) cc ON c.committee_id = cc.committee_id
         LEFT JOIN
     (SELECT committee_id,
             address,
             city,
             state,
             postcode,
             phone
      FROM committee_contact
      WHERE address IS NOT NULL
        AND ROLE = 'committee') cc2
     ON c.committee_id = cc2.committee_id
WHERE c.committee_type = 'CO'
  AND Cast(:election_code AS INT) BETWEEN c.start_year
    AND Coalesce(
            c.end_year,
            Cast(:election_code AS INT)
        );