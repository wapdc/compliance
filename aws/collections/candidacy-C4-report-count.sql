with general as (
    select
        x.election_code, 'General' as election_type,
        x.reporting_period_type,
        count(distinct case when (x.filed and x.reporting_type = 'full' ) then x.candidacy_id end) as filed_count,
        count(distinct case when x.reporting_type = 'full' then x.candidacy_id end) as full_count
    from (
             select
                 ca.candidacy_id, ca.election_code, min(c.pac_type), min(c.reporting_type),  min(r.period_start), max(r.period_end), rp.reporting_period_type, rp.reporting_period_id,
                 case when c.committee_id is null and min(j.rpts) = 'C' then 'full'
                     when c.committee_id is null then null
                     else cf_reporting_status(c.committee_id, rp.start_date) end as reporting_type,
                 case when (min(r.period_start) <= min(rp.start_date)) and (max(r.period_end) >= max(rp.end_date)) then true else false end as filed
             from
                 candidacy ca
                  join election e on ca.election_code = e.election_code
                  join reporting_period rp on rp.report_type = 'C4' and rp.reporting_period_type in ('PostGeneral', 'SevenDayPreGeneral', 'TwentyOneDayPreGeneral')
                     and e.election_date = rp.election_date
                  join jurisdiction j on ca.jurisdiction_id = j.jurisdiction_id
                  left join committee c on ca.committee_id = c.committee_id
                              left join fund f on c.committee_id = f.committee_id
                     and f.election_code > '2018' and f.election_code = e.election_code
                              left join report r on f.fund_id = r.fund_id
                     and ((rp.start_date between r.period_start and r.period_end) or (rp.end_date between r.period_start and r.period_end))
                     and r.submitted_at < coalesce(cast($1 as date), now())
             where
               ((c.committee_id is null and j.rpts='C') or (c.committee_id is not null and cf_reporting_status(c.committee_id, rp.start_date) = 'full'))
               and ca.exit_reason is null
               and ca.declared_date is not null
               and ca.in_general
             group by
                 ca.election_code, ca.candidacy_id,  rp.reporting_period_type, rp.reporting_period_id, c.committee_id
         ) x
    group by
        x.election_code, x.reporting_period_type
),
     primary_elections as (
         select
             x.election_code, 'Primary' as election_type,
             x.reporting_period_type,
             count(distinct case when (x.filed  and x.reporting_type = 'full' ) then x.candidacy_id end) as filed_count,
             count(distinct case when x.reporting_type = 'full' then x.candidacy_id end) as full_count
         from (
                 select
                     ca.candidacy_id, ca.election_code, min(c.pac_type), min(c.reporting_type),  min(r.period_start), max(r.period_end), rp.reporting_period_type, rp.reporting_period_id,
                     case when c.committee_id is null and min(j.rpts) = 'C' then 'full'
                          when c.committee_id is null then null
                          else cf_reporting_status(c.committee_id, rp.start_date) end as reporting_type,
                     case when (min(r.period_start) <= min(rp.start_date)) and (max(r.period_end) >= max(rp.end_date)) then true else false end as filed
                 from
                     candidacy ca
                        join election e on concat(ca.election_code,'P') = e.election_code
                        join reporting_period rp on rp.report_type = 'C4' and rp.reporting_period_type in ('PostPrimary', 'SevenDayPrePrimary', 'TwentyOneDayPrePrimary')
                           and e.election_date = rp.election_date
                        join jurisdiction j on j.jurisdiction_id= ca.jurisdiction_id
                        left join committee c on ca.committee_id = c.committee_id
                             left join fund f on c.committee_id = f.committee_id
                         and f.election_code > '2018' and f.election_code = substring(e.election_code, 1, 4)
                             left join report r on f.fund_id = r.fund_id and
                            ((rp.start_date between r.period_start and r.period_end) or (rp.end_date between r.period_start and r.period_end))
                             and r.submitted_at < coalesce(cast($1 as date), now())
                 where
                   ((c.committee_id is null and j.rpts='C') or (c.committee_id is not null and cf_reporting_status(c.committee_id, rp.start_date) = 'full'))
                   and ca.exit_reason is null
                   and ca.declared_date is not null
                   and ca.in_primary
                 group by
                     ca.election_code, ca.candidacy_id,  rp.reporting_period_type, rp.reporting_period_id, c.committee_id
             ) x
         group by x.election_code, x.reporting_period_type
     ),
     combined_reports as (
         select
             ca.election_code,
             json_build_object(
                     'election_type', ca.election_type,
                     'Post', sum(case when ca.reporting_period_type like 'Post%' then ca.filed_count else 0 end),
                     'PostFullCount', sum(case when ca.reporting_period_type like 'Post%' then ca.full_count else 0 end),
                     'TwentyOneDay', sum(case when ca.reporting_period_type like 'TwentyOneDay%' then ca.filed_count else 0 end),
                     'TwentyOneDayFullCount', sum(case when ca.reporting_period_type like 'TwentyOneDay%' then ca.full_count else 0 end),
                     'SevenDay', sum(case when ca.reporting_period_type like 'SevenDay%' then ca.filed_count else 0 end),
                     'SevenDayFullCount', sum(case when ca.reporting_period_type like 'SevenDay%' then ca.full_count else 0 end)
             ) as report
         from (
                  select * from general
                  union all
                  select * from primary_elections
              ) ca

         where ca.election_code <= extract(year from coalesce(cast($1 as date), now()))::text
         group by ca.election_code, ca.election_type
         order by ca.election_code,ca.election_type desc
     ),
     final_agg as (
         select election_code,json_agg(report) as reports from combined_reports
         group by election_code
     )
select election_code,json_build_object('reports', reports) as report_counts from final_agg
order by election_code desc
