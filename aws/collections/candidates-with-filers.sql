SELECT
    c.candidacy_id,
    cm.member_id,
    CASE
        WHEN c.candidacy_id is null then true
        WHEN c.filer_id is not null then true
        WHEN c.exit_reason in ('deceased', 'withdrew') then true
        ELSE false END published
FROM collection_member cm
         LEFT JOIN candidacy c ON cm.target_id = c.candidacy_id
WHERE collection_id = CAST(:collection_id AS INTEGER)
order by target_id;