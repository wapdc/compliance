select c.candidacy_id,
       p.name,
       COALESCE(c.filer_id, p.filer_id) as filer_id,
       c.general_result,
       c.election_code,
       COALESCE(c.address, p.address) as address,
       CASE WHEN c.address is not null THEN c.city else p.city end city,
       case when c.address is not null then c.STATE ELSE P.state end state,
       CASE WHEN c.address is not null THEN c.postcode else p.postcode end postcode,
       COALESCE(c.declared_email,p.email) AS email,
       p.phone
from candidacy c
         join person p on c.person_id = p.person_id
where election_code = :election_code
  and (c.primary_result is not null or c.general_result is not null)