select
    c.candidacy_id,
    coalesce(cc.contact_emails, c.declared_email) as email,
    case when c.committee_id is null then 'registration' else 'C4' end as flag,
    c.address,
    c.city,
    c.state,
    c.postcode,
    c.exit_reason,
    p.name,
    p.phone
from (
         select
             ca.*, min(r.period_start), max(r.period_end)
         from candidacy ca
                  join jurisdiction j on j.jurisdiction_id = ca.jurisdiction_id
                  join reporting_period rp on rp.report_type = 'C4' and rp.reporting_period_id = :reporting_period_id
                  left join committee c on ca.committee_id = c.committee_id
                  left join fund f on c.committee_id = f.committee_id
                  left join report r on f.fund_id = r.fund_id and r.report_type = 'C4'
             and ((rp.start_date between r.period_start and r.period_end) or (rp.end_date between r.period_start and r.period_end))
         where ca.election_code = cast(:election_code as varchar)
           and ((c.committee_id is null and j.rpts = 'C') or (c.committee_id is not null and cf_reporting_status(ca.committee_id, rp.start_date) = 'full'))
           and ca.declared_date is not null
           and ca.in_primary = true
           and ca.exit_reason is null
         group by ca.candidacy_id, c.committee_id
         having ((min(r.period_start) is null) or min(r.period_start) > (greatest(min(rp.start_date),min(ca.campaign_start_date)))) or
             ((max(r.period_end) is null) or max(r.period_end) < max(rp.end_date))
     ) c
         join person p ON c.person_id = p.person_id
         left join ( select committee_id, string_agg(distinct lower(email), ';')  as contact_emails from committee_contact
                     where treasurer = true or role = 'committee' group by committee_id
         ) cc on c.committee_id = cc.committee_id