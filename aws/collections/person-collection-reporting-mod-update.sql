select p.person_id, p.name, p.email, true as excluded
from collection_member cm
join person p on p.person_id = cm.target_id
join fa_statement fa on fa.person_id = p.person_id
join reporting_modification rm on fa.person_id = rm.target_id
    and target_type = 'person'
    and rm.decision_code = 'approved'
    and rm.modification_type = 'residential_address'
    and rm.start_date <= fa.period_end
    and coalesce(rm.end_date, fa.period_end) >= fa.period_end
where cm.collection_id = cast(:collection_id as INTEGER)
  and extract(year from fa.period_start) = :year