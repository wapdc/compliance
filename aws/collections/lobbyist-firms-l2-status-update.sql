select
    f.name,
    trim (COALESCE(f.address_1,'') || ' ' || coalesce(f.address_2,'')) as address,
    f.city,
    f.state,
    f.postcode,
    f.email,
    f.phone,
    f.lobbyist_firm_id,
    coalesce(r.metadata, json_build_object('name', f.name, 'reports', null)) as metadata,
    coalesce(case when r.metadata is null then true end, r.exclude, m.excluded) as excluded
from collection_member m
    join lobbyist_firm f on m.target_id = f.lobbyist_firm_id
        left join
    (select lr.lobbyist_firm_id,
             case
               when
                   Coalesce(cast(:is_not_filed as bool), false)
                       and (count(case when not lr.exempt then lr.lobbyist_firm_id end) = count(case when not lr.exempt then l2.report_id end))
                 and (not coalesce(cast(:is_submitted_late as bool), false) or (bool_and((not lr.exempt) and l2s.first_submitted >= rp.due_date)))
                 and (not coalesce(cast(:is_amended_late as bool), false ) or (bool_and((not lr.exempt) and l2s.last_submitted >= rp.due_date)))
                 then true
                 end as exclude,
             json_build_object(
                                'name', max(f.name),
                                'reports', json_agg(json_build_object(
                                                           'm', cast(to_char(lr.period_start, 'MM') as int),
                                                           'period', to_char(lr.period_start, 'Month YYYY'),
                                                           'period_start', lr.period_start,
                                                           'status', case when l2.report_id is not null then 'filed'
                                                               when lr.exempt then 'exempt' else 'missing' end,
                                                           'is_filed', case when l2.report_id is not null then true else false end,
                                                           'first_submitted_at', l2s.first_submitted,
                                                           'last_submitted_at', l2s.last_submitted,
                                                           'is_submitted_late', case when (l2s.first_submitted > rp.due_date) then true end,
                                                           'is_amended_late', case when (l2s.last_submitted > rp.due_date) then true end
                                                   ) order by lr.period_start)) as metadata
      from
          lobbyist_firm f
              left join (select lrp.lobbyist_firm_id, lrp.period_start, lrp.period_end, bool_and(lrp.exempt) as exempt from lobbyist_reporting_periods lrp group by lrp.lobbyist_firm_id, lrp.period_start, lrp.period_end) lr on lr.lobbyist_firm_id = f.lobbyist_firm_id
              left join l2 on l2.lobbyist_firm_id=lr.lobbyist_firm_id and l2.period_start=lr.period_start
              left join (select report_id, min(submitted_at) first_submitted, max(submitted_at) last_submitted from l2_submission s group by report_id) l2s on l2s.report_id=l2.report_id
              left join reporting_period rp on rp.start_date = lr.period_start and rp.report_type = 'L2'
      where lr.period_start between cast(:year || '-01-01' as date) and cast(:year || '-12-31' as date)
        and lr.period_end<= now()
        and extract(month  from lr.period_start) between cast(:start_month as int) and cast(:end_month as int)
      group by lr.lobbyist_firm_id
     ) r on r.lobbyist_firm_id = m.target_id
where m.collection_id = :collection_id
