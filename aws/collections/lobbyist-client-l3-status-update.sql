select
    c.name,
    trim (COALESCE(c.address_1,'') || ' ' || coalesce(c.address_2,'')) as address,
    c.city,
    c.state,
    c.postcode,
    c.email,
    c.phone,
    c.lobbyist_client_id,
    cc.metadata,
    case when cast(:is_not_filed as bool) = true and r.report_id is not null then true
    else m.excluded end as excluded
from
    collection_member m
    join lobbyist_client c on c.lobbyist_client_id=m.target_id
    join (select fc.lobbyist_client_id,
            fc.start_date,
            max(fc.due_date) as due_date,
            json_build_object(
                         'name', max(fc.client_name),
                         'due_date', to_char(max(fc.due_date), 'YYYY-MM-DD'),
                         'reports', json_agg(
                                 json_build_object(
                                         'lobbyist_firm_id', fc.lobbyist_firm_id,
                                         'email', fc.email,
                                         'lobbyist_firm', fc.name
                                 )
                        )
                 ) as metadata
    from (select rp.start_date,
              lrp.lobbyist_client_id,
              lrp.lobbyist_firm_id,
              max(lf.email) as email,
              max(lc.name) as client_name,
              max(rp.due_date) as due_date,
              max(lf.name)     as name
           from reporting_period rp
                    join lobbyist_reporting_periods lrp on lrp.period_start between rp.start_date and rp.end_date
                    join lobbyist_firm lf on lrp.lobbyist_firm_id = lf.lobbyist_firm_id
                    join lobbyist_client lc ON lrp.lobbyist_client_id = lc.lobbyist_client_id
           where extract(year from start_date) = cast(:year as int)
             and report_type = 'L3'
           group by lrp.lobbyist_firm_id, lrp.lobbyist_client_id, rp.start_date, rp.end_date) fc
    group by fc.lobbyist_client_id, fc.start_date) cc
    on c.lobbyist_client_id=cc.lobbyist_client_id
 left join (
   select l3.lobbyist_client_id, l3.report_id, min(l3s.submitted_at) as min_submitted_at, max(l3s.submitted_at)
     from l3 JOIN l3_submission l3s on l3s.report_id = l3.report_id
     where  l3.report_year = cast(:year as int)
     group by l3.lobbyist_client_id, l3.report_id
 ) r ON r.lobbyist_client_id = c.lobbyist_client_id
where m.collection_id = cast(:collection_id as int)