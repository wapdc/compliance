select
    cm.target_id as candidacy_id,
    cm.label,
    case
        when cm.excluded then cm.excluded
        when x.report_id is not null then true
        when ca.exit_reason is not null then true
        else false
        end as excluded,
    case
        when x.report_id is not null then 'filed'
        when ca.exit_reason is not null then ca.exit_reason
        else cm.flag
        end as flag
from
     collection_member cm
        join candidacy ca on ca.candidacy_id = cm.target_id
        join committee c on c.committee_id = ca.committee_id
        left join (
        select
            f.committee_id, min(r.period_start), max(r.period_end), max(report_id) as report_id
        from fund f
                left join report r on f.fund_id = r.fund_id and r.report_type = 'C4' and r.superseded_id is null
                join reporting_period rp on rp.report_type = 'C4' and rp.reporting_period_id = :reporting_period_id
                join candidacy ca on ca.committee_id = f.committee_id
        where
            (rp.start_date between r.period_start and r.period_end) or (rp.end_date between r.period_start and r.period_end)
        group by
            f.committee_id
        having
            (min(r.period_start) <= greatest(min(rp.start_date), min(ca.campaign_start_date))) and (max(r.period_end) >= max(rp.end_date))
        ) x on c.committee_id = x.committee_id
where
    ca.election_code = cast(:election_code as varchar) and cm.collection_id = :collection_id
    and (x.report_id is not null or ca.exit_reason is not null)
    and cm.excluded is distinct from true