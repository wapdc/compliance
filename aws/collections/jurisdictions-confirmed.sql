SELECT
    j.jurisdiction_id,
    jc.email,
    jc.address,
    jc.premise,
    jc.city,
    jc.state,
    jc.postcode,
    jc.phone,
    CASE
        WHEN cm.excluded THEN cm.excluded
        WHEN j.officials_confirmed_at is not null and j.officials_confirmed_at > :confirmed_date THEN true
        ELSE false END confirmed
FROM collection_member cm
         JOIN jurisdiction j ON cm.target_id = j.jurisdiction_id
         JOIN jurisdiction_contact jc ON jc.jurisdiction_id = cm.target_id
WHERE collection_id = CAST(:collection_id AS INTEGER)
order by target_id;