select lc.name,
       trim(COALESCE(lc.address_1, '') || ' ' || coalesce(lc.address_2, '')) as address,
       lc.city,
       lc.state,
       lc.postcode,
       lc.email,
       lc.phone,
       lc.lobbyist_client_id,
       r.min_submitted_at >= cc.due_date                       as is_submitted_late,
       r.max_submitted_at >= cc.due_date  as amended_late,
       cc.due_date,
       extract(year from cc.start_date)                                      as year,
       cc.metadata
from lobbyist_client as lc
         join
     (select fc.lobbyist_client_id,
             fc.start_date,
             max(fc.due_date)  as due_date,
             json_build_object(
                     'name', max(fc.client_name),
                     'due_date', to_char(max(fc.due_date), 'YYYY-MM-DD'),
                     'reports', json_agg(
                             json_build_object(
                                     'lobbyist_firm_id', fc.lobbyist_firm_id,
                                     'email', fc.email,
                                     'lobbyist_firm', fc.name
                             )
                                )
             ) as metadata
      from (select rp.start_date,
                   lrp.lobbyist_client_id,
                   lrp.lobbyist_firm_id,
                   max(lf.email) as email,
                   max(lc.name) as client_name,
                   max(rp.due_date) as due_date,
                   max(lf.name)     as name
            from reporting_period rp
                     join lobbyist_reporting_periods lrp on lrp.period_start between rp.start_date and rp.end_date
                     join lobbyist_firm lf on lrp.lobbyist_firm_id = lf.lobbyist_firm_id
                     join lobbyist_client lc ON lrp.lobbyist_client_id = lc.lobbyist_client_id
            where extract(year from start_date) = cast(:year as int)
              and report_type = 'L3'
            group by lrp.lobbyist_firm_id, lrp.lobbyist_client_id, rp.start_date, rp.end_date) fc
      group by fc.lobbyist_client_id, fc.start_date) cc
     on lc.lobbyist_client_id = cc.lobbyist_client_id
  left join (
    select l3.lobbyist_client_id, l3.report_id, min(l3s.submitted_at) as min_submitted_at, max(l3s.submitted_at) as max_submitted_at
    from l3 JOIN l3_submission l3s on l3s.report_id = l3.report_id
    where  l3.report_year = cast(:year as int)
    group by l3.lobbyist_client_id, l3.report_id
) r ON r.lobbyist_client_id = lc.lobbyist_client_id
where (cast(:is_not_filed as bool) = true and r.report_id is null)
  OR (cast(:is_submitted_late as bool) = true and r.min_submitted_at >= (cc.due_date + interval '1 day'))
  OR (cast(:is_amended_late as bool) = true and r.max_submitted_at >= (cc.due_date + interval '1 day'))
