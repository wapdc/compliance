select
  j.*,
  jc.email,
  jca.address,
  jca.premise,
  jca.city,
  jca.state,
  jca.postcode,
  jca.phone,
  json_build_object('jurisdiction_contact_id', jca.jurisdiction_contact_id) as metadata
from jurisdiction j
  join (select jcg.jurisdiction_id, string_agg(jcg.email, ';') as email  from jurisdiction_contact jcg
     group by jcg.jurisdiction_id) jc on j.jurisdiction_id = jc.jurisdiction_id
  left join
    (select jc2.*, row_number() over (partition by jc2.jurisdiction_id order by jc2.updated_at desc ) r
     from jurisdiction_contact jc2 where address is not null) jca
    on jca.jurisdiction_id=j.jurisdiction_id and r=1

where (j.rpts = 'C' or j.rpts = 'F')
  and coalesce(j.valid_to, current_date) >= current_date
  and j.inactive is null
  and (j.officials_confirmed_at is null or j.officials_confirmed_at < :confirmed_date);