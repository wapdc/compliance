SELECT
  c.candidacy_id,
  c.committee_id,
  COALESCE(c.ballot_name, p.name) AS NAME,
  coalesce(ct.email, c.declared_email, p.email) as email,
  p.address,
  p.city,
  p.state,
  p.postcode,
  p.phone
FROM candidacy c
       join person p on c.person_id=p.person_id
       left join committee com
                 on com.committee_id = c.committee_id
       left join committee_contact ct
                 on ct.committee_id = c.committee_id
                   and ct.role = 'committee'
where c.position_id is null
  and c.election_code = cast (:election_code as varchar)
  AND c.exit_reason IS NULL
  AND p.inactive_date IS NULL
  AND ( c.office_code, c.jurisdiction_id)
  in (select offcode, jurisdiction_id
      FROM jurisdiction_office jo
             join position p on p.jurisdiction_office_id = jo.jurisdiction_office_id
      group by jo.offcode, jo.jurisdiction_id
      having count(1) > 1);