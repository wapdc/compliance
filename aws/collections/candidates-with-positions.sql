SELECT
  c.candidacy_id,
  c.committee_id,
  COALESCE(c.ballot_name, p.name) AS name,
  coalesce(ct.email, c.declared_email, p.email) as email,
  CASE WHEN c.position_id is not null then 'filed'
    when c.exit_reason is not null then 'exited race' end as flag,
  true as excluded
FROM collection_member cm
       join candidacy c on c.candidacy_id = cm.target_id
       join person p on c.person_id=p.person_id
       left join committee com
                 on com.committee_id = c.committee_id
       left join committee_contact ct
                 on ct.committee_id = c.committee_id
                   and ct.role = 'committee'
where
  (c.position_id is not null or c.exit_reason is not null)
  and collection_id = CAST(:collection_id AS INTEGER)
  and c.election_code = cast (:election_code as varchar)
  AND p.inactive_date IS NULL
  AND ( c.office_code, c.jurisdiction_id)
  in (select offcode, jurisdiction_id
      FROM jurisdiction_office jo
             join position p on p.jurisdiction_office_id = jo.jurisdiction_office_id
      group by jo.offcode, jo.jurisdiction_id
      having count(1) > 1);