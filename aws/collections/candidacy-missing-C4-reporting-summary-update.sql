select
    cm.target_id as candidacy_id,
    cm.label,
    case
        when cm.excluded then cm.excluded
        when
            (
                (cast(:is_not_filed as bool) = true and bool_and(x.is_filed = true or x.reporting_type = 'mini'))
                    or (cast(:is_submitted_late as bool) = true and bool_and(x.first_submitted_at < x.due_date))
                    or (cast(:is_amended_late as bool) = true and bool_and(x.last_submitted_at < x.due_date))
            )
            then true
        else false
        end as excluded,
    jsonb_build_object(
            'reports', jsonb_agg( jsonb_build_object(
            'reporting_period_type', x.reporting_period_type,
            'reporting_period_id', x.reporting_period_id,
            'period_start', x.start_date,
            'period_end', x.end_date,
            'is_filed', x.is_filed,
            'reporting_type', x.reporting_type,
            'first_submitted_at', x.first_submitted_at,
            'last_submitted_at', x.last_submitted_at
                                          )
                                  order by x.start_date
                       )
    ) as metadata
from
    collection_member cm
        join candidacy ca on ca.candidacy_id = cm.target_id
        join committee c on c.committee_id = ca.committee_id
        left join (
        select
            ca.candidacy_id,
            rp.reporting_period_id,
            rp.reporting_period_type,
            rp.start_date,
            rp.end_date,
            case when c.committee_id is null and j.rpts = 'C' then 'full' when c.committee_id is null then null else cf_reporting_status(c.committee_id, rp.start_date) end as reporting_type,
            min(r.report_id) as report_id,
            min(r.submitted_at) as first_submitted_at,
            max(r.submitted_at) as last_submitted_at,
            rp.due_date,
            c.committee_id,min(r.period_start) as period_start,max(r.period_end) as period_end,
            case
                when
                     ((min(r.period_start) <= greatest(min(rp.start_date), min(ca.campaign_start_date)))
                        and (max(r.period_end) >= max(rp.end_date)))
                    then true
                else false
                end as is_filed
        from candidacy ca
                 join reporting_period rp on rp.report_type = 'C4' and rp.reporting_period_type in ('PostGeneral', 'SevenDayPreGeneral', 'TwentyOneDayPreGeneral')
            and extract(year from rp.start_date) = cast(:year as int)
                 join jurisdiction j on ca.jurisdiction_id = j.jurisdiction_id
                 left join committee c on ca.committee_id = c.committee_id
                 left join fund f on c.committee_id = f.committee_id
                 left join report r on  f.fund_id = r.fund_id and ((rp.start_date between r.period_start and r.period_end) or (rp.end_date between r.period_start and r.period_end))
            and r.superseded_id is null
            group by ca.candidacy_id, rp.reporting_period_id, rp.reporting_period_type, rp.start_date, rp.end_date, rp.due_date, c.committee_id, j.rpts
                ) x on c.committee_id = x.committee_id
where cm.collection_id = :collection_id and cm.target_id = ca.candidacy_id
group by  cm.target_id, cm.label, cm.excluded
