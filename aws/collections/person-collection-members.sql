SELECT p.name,
       p.filer_id,
       p.person_id,
       p.sortcode,
       cm.address,
       cm.city,
       cm.state,
       cm.postcode,
       cm.phone,
       cm.member_id,
       excluded,
       cm.target_id,
       cm.compare_id,
       cm.email,
       cm.memo,
       cm.flag,
       cm.last_contacted
FROM collection_member cm
       JOIN person p ON cm.target_id = p.person_id
WHERE cm.collection_id = :collection_id
ORDER BY p.name