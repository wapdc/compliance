select
    f.name,
    trim (COALESCE(f.address_1,'') || ' ' || coalesce(f.address_2,'')) as address,
    f.city,
    f.state,
    f.postcode,
    f.email,
    f.phone,
    f.lobbyist_firm_id,
    r.metadata

from (select lr.lobbyist_firm_id,
             json_build_object(
                     'name',max(f.name),
                     'clients', json_agg(clients)->0,
                     'due_date', to_char(max(rp.due_date), 'MM/DD/YYYY')
             ) as metadata
      from
          (select
               lrp.lobbyist_firm_id,
               lrp.period_start,
               lrp.period_end,
               json_agg(json_build_object( 'name', lc.name) order by lc.name) as clients
             from lobbyist_reporting_periods lrp
               join lobbyist_contract c on c.lobbyist_contract_id = lrp.lobbyist_contract_id
               join lobbyist_client lc on lc.lobbyist_client_id = c.lobbyist_client_id
             group by lrp.lobbyist_firm_id, lrp.period_start, lrp.period_end
             having bool_and(exempt) = false

          ) lr
              left join reporting_period rp on rp.start_date = lr.period_start and rp.report_type = 'L2'
              left join l2 on l2.lobbyist_firm_id=lr.lobbyist_firm_id and l2.period_start=lr.period_start
              left join (select report_id, min(submitted_at) first_submitted, max(submitted_at) last_submitted from l2_submission s group by report_id) l2s on l2s.report_id=l2.report_id
              left join lobbyist_firm f on f.lobbyist_firm_id = lr.lobbyist_firm_id
      where lr.period_start between cast(:year || '-01-01' as date) and cast(:year || '-12-31' as date)
        and lr.period_end<= now()
        and extract(month  from lr.period_start) = cast(:month as int)
      group by lr.lobbyist_firm_id
      having (count(lr.lobbyist_firm_id) <> count(l2.report_id))
     ) r
         join lobbyist_firm f on f.lobbyist_firm_id=r.lobbyist_firm_id

