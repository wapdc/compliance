select
    cm.target_id as candidacy_id,
    true as excluded,
    case
        when ca.exit_reason is not null then ca.exit_reason
        else 'mini' end as flag
from
    collection_member cm
        join candidacy ca on ca.candidacy_id = cm.target_id
        join committee c on c.committee_id = ca.committee_id
        join reporting_period rp on rp.report_type = 'C4' and rp.reporting_period_id = :reporting_period_id
where
    cm.collection_id = :collection_id
    and c.registered_at > rp.start_date and c.reporting_type = 'mini'