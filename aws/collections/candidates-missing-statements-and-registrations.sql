SELECT
  p.name,
  COALESCE(ca.ballot_name, p.name) as ballot_name,
  ca.election_code,
  ca.person_id,
  ca.candidacy_id,
  coalesce(ca.declared_email, p.email, cc.email) as declared_email,
  ca.filer_id,
  j.rpts,
  ca.committee_id,
  ca.fa_statement_id,
  ca.sos_candidate_id,
  ca.sos_candidate_code,
  coalesce(ca.address, p.address, cc.address) as address,
  case
    when ca.address is not null then ca.city
    when p.address is not null then p.city
    when cc.address is not null then cc.city
  end as city,
  case
    when ca.address is not null then ca.state
    when p.address is not null then p.state
    when cc.address is not null then cc.state
  end as state,
  case
    when ca.address is not null then ca.postcode
    when p.address is not null then p.postcode
    when cc.address is not null then cc.postcode
  end as postcode,
  coalesce(p.phone, cc.phone) as phone,
  CASE
    WHEN ca.fa_statement_id IS NULL and ca.committee_id is null AND j.rpts = 'C' then 'both'
    WHEN ca.committee_id is null AND j.rpts='C' THEN 'registration'
    WHEN ca.fa_statement_id IS NULL and (j.rpts IN ('F','C') or c.reporting_type = 'full') THEN 'financial'
  END reports_missing
FROM candidacy ca
  JOIN person p ON ca.person_id=p.person_id
  JOIN jurisdiction j ON ca.jurisdiction_id = j.jurisdiction_id
  left join committee c on c.committee_id=ca.committee_id
  left join committee_contact cc
    on cc.committee_id = ca.committee_id
      and cc.role = 'candidacy'
WHERE ca.election_code = :election_code
  AND (
    (ca.committee_id IS NULL AND j.rpts = 'C')
      OR (ca.fa_statement_id IS NULL AND j.rpts in ('C', 'F')
      OR (c.reporting_type = 'full' and ca.fa_statement_id is null)    )
  )
  AND ca.exit_reason IS NULL
  AND ca.primary_result is distinct from 'C'
ORDER BY p.name