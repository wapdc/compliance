update candidacy c set fa_statement_id = statement_id from (
  -- It is important that this logic match logic in CandidacyProcessor::getQualifyingStatementID
  select candidacy_id, min(statement_id) as statement_id, max(p.name) as name from candidacy c
      join person p on c.person_id=p.person_id
      left join fa_statement fa on c.person_id = fa.person_id
    where fa_statement_id is null
        and (
                date_part('year', first_filed_at) = date_part('year', c.campaign_start_date)
            or (
                            c.campaign_start_date - interval '1 day' between period_start and period_end
                        and c.campaign_start_date - interval '1 year' between period_start and period_end
                    )
        )
      and c.campaign_start_date - interval '1 year' <= period_end
        and election_code= :election_code
    group by (candidacy_id)) v
where v.candidacy_id = c.candidacy_id
