select
    cm.target_id as committee_id,
    case
        when cm.excluded then cm.excluded
        when x.report_id is not null then true
        else false
        end as excluded,
    case
        when x.report_id is not null then 'filed'
        else cm.flag
        end as flag
from
    collection_member cm
        join committee c on c.committee_id = cm.target_id
        join committee_relation cr on c.committee_id = cr.committee_id
        join proposal pr on cr.target_id = pr.proposal_id
        left join (
        select
            f.committee_id, min(r.period_start) as period_start, max(r.period_end) as period_end, max(r.report_id) as report_id
        from fund f
                 join reporting_period rp on rp.report_type = 'C4' and rp.reporting_period_id = :reporting_period_id
                 left join report r on f.fund_id = r.fund_id and r.superseded_id is null and r.report_type = 'C4'
        where
            (rp.start_date between r.period_start and r.period_end) or (rp.end_date between r.period_start and r.period_end)
        group by
            f.committee_id
        having
            (min(r.period_start) <= min(rp.start_date)) and (max(r.period_end) >= max(rp.end_date))
    ) x on c.committee_id = x.committee_id
where pr.election_code = cast(:election_code as varchar) and cm.collection_id = :collection_id

