SELECT
  ca.candidacy_id,
  CASE
    WHEN ca.exit_reason IS NOT NULL then ca.exit_reason
    WHEN ca.primary_result = 'C' THEN 'certified'
    WHEN fa_statement_id IS NULL and ca.committee_id is null AND j.rpts = 'C' THEN 'both'
    WHEN ca.committee_id is null AND j.rpts='C' THEN 'registration'
    WHEN fa_statement_id IS NULL and (j.rpts IN ('F','C') or c.reporting_type='full') THEN 'financial'
    WHEN p.inactive_date IS NOT NULL THEN 'inactive/deceased'
    ELSE 'filed' END reports_missing,
  CASE
    WHEN cm.excluded THEN cm.excluded
    WHEN p.inactive_date IS NOT NULL THEN TRUE
    WHEN ca.exit_reason IS NOT NULL THEN TRUE
    WHEN ca.primary_result = 'C' THEN TRUE
    WHEN fa_statement_id IS NULL and ca.committee_id is null AND j.rpts = 'C' THEN false
    WHEN ca.committee_id is null AND j.rpts='C' THEN false
    WHEN fa_statement_id IS NULL and (j.rpts IN ('F','C') or c.reporting_type='full') THEN false
    ELSE true END filed,
  j.rpts,
  ca.committee_id, ca.fa_statement_id
FROM collection_member cm
  JOIN candidacy ca  ON cm.target_id=ca.candidacy_id
  JOIN jurisdiction j ON CA.jurisdiction_id = J.jurisdiction_id
  JOIN person p ON ca.person_id = p.person_id
  left join committee c on ca.committee_id=c.committee_id
WHERE ca.election_code = :election_code
  and collection_id = CAST(:collection_id AS INTEGER)

