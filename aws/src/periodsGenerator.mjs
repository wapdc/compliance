/**
 * Adjust the date for weekends and public holidays for the L3 reports
 * @param date
 * @returns {Date}
 */
function adjustForWeekendsAndHolidays(date) {
    let dueDate = new Date(date);
    let day = dueDate.getDay();
    if (day === 0) {
        dueDate.setDate(dueDate.getDate() + 1);
    }
    if (day === 6) {
        dueDate.setDate(dueDate.getDate() + 2);
    }
    return dueDate;
}

/**
 * Public holidays
 * @type {string[]}
 */
const publicHolidays = [
    "2024-01-15",
    "2025-02-17",
    "2026-02-16",
    "2027-02-15",
    "2028-01-17",
    "2029-01-15",
    "2031-02-17",
    "2032-02-16",
    "2033-01-17",
    "2034-01-16",
    "2035-01-15",
    "2035-02-17",
    "2036-02-16",
    "2037-02-15",
    "2038-01-17",
    "2039-01-16",
    "2040-02-17",
    "2041-02-16",
    "2042-02-15",
    "2043-01-17",
    "2044-01-16",
    "2045-02-17",
    "2046-02-16",
    "2047-02-15",
    "2048-01-17",
    "2049-01-16",
    "2050-02-17"

];

/**
 * Check if the date is a public holiday
 * @param date
 * @returns {boolean}
 */
function isPublicHoliday(date) {
    return publicHolidays.includes(date.toISOString().split('T')[0]);
}

/**
 * Adjust the date for weekends and public holidays for the L2 reports
 * @param date
 * @returns {Date}
 */
function adjustForWeekendsAndHolidaysL2(date) {
    let adjustedDate = new Date(date);
    while (adjustedDate.getDay() === 0 || adjustedDate.getDay() === 6 || isPublicHoliday(adjustedDate)) {
        adjustedDate.setDate(adjustedDate.getDate() + 1);
    }
    return adjustedDate;
}

/**
 * Format the date string
 * @param date
 * @returns {*}
 */
function formatDateString(date) {
    date = validateDate(date);
    return date.toISOString().split('T')[0];
}

function getLastDayOfFebruary(year) {
    return new Date(year, 2, 0).getDate();
}

function validateDate(date) {
    // If the input is already a valid Date object, return it
    if (date instanceof Date && !isNaN(date.getTime())) {
        return date;
    }

    // If the input is a string, try to parse it as a date
    if (typeof date === 'string') {
        const parsedDate = new Date(date);
        if (!isNaN(parsedDate.getTime())) {
            return parsedDate;
        }
    }

    // If the input is neither a valid Date object nor a valid date string, return null
    return null;
}

/**
 * Calculate the days after the date
 * The type parameter is used to check if the date is a leap year. It then can perform the necessary calculations
 * @param date
 * @param days
 * @param type
 * @returns {*}
 */
function calculateDaysAfter(date, days, type) {
    const _date = date.toISOString().split('T')[0]
    const _month = date.toISOString().split('T')[0].split('-')[1]
    if(_month === '02' && new Date(new Date(_date).getFullYear(), 2, 0).getDate()
        && type === "leapYearCheck") {
        days = parseInt(days) + 1;
    }
    days = parseInt(days)
    return formatDateString(new Date(date.setDate(date.getDate() + days)));
}

/**
 * Calculate the days before the date
 * @param date
 * @param days
 * @param type
 * @returns {*}
 */
function calculateDaysBefore(date, days, type) {
    if(date.toISOString().split('T')[0].split('-')[1] === '02') {
        if (new Date(new Date().getFullYear(), 2, 0).getDate() === 29 && type === "leapYearCheck") {
            days = parseInt(days) + 1;
        }
    }
    days = parseInt(days)
    return formatDateString(new Date(date.setDate(date.getDate() - days)));
}

/**
 * Generate the report periods for the L2 reports
 * @param start_year
 * @param end_year
 * @param report_type
 * @returns {*[]}
 */
function generateL2ReportPeriods(start_year, end_year, report_type) {
    start_year = parseInt(start_year);
    end_year = parseInt(end_year);
    let data = [];
    for (let year = start_year; year <= end_year; year++) {
        for (let month = 0; month < 12; month++) {
            let dueDate = new Date(year, month + 1, 15);
            dueDate = adjustForWeekendsAndHolidaysL2(dueDate);
            const dueDateFormat = dueDate.toISOString().split('T')[0];
            let row = {
                "report_type": report_type,
                "reporting_period_type": "Monthly",
                "start_date": formatDateString(new Date(year, month, 1)),
                "end_date": formatDateString(new Date(year, month + 1, 0)),
                "due_date": dueDateFormat,
                "reminder_date": formatDateString(new Date(year, month, 1)),
                "reminder_set_date": null
            };
            data.push(row);
        }
    }
    return data;
}

/**
 * Generate the report periods for the L3 reports
 * @param start_year
 * @param end_year
 * @param report_type
 * @returns {*[]}
 */
function generateL3ReportPeriods(start_year, end_year, report_type) {
    start_year = parseInt(start_year);
    end_year = parseInt(end_year);
    let data = [];
    for (let year = start_year; year <= end_year; year++) {
           let row = {
            "report_type": report_type,
            "reporting_period_type": "Annual",
            
            "start_date": year + "-01" + "-01",
            "end_date": year + "-12" + "-31",
            "due_date": (adjustForWeekendsAndHolidays(new Date(year, 2, 0))).toISOString().split('T')[0],
            "reminder_date": year + "-01" + "-01",
            "reminder_set_date": null
        };
        data.push(row);
    }
    return data;
}

/**
 * Generate the report periods for the C4 report period dates
 * @param february_date
 * @param april_date
 * @param primary_date
 * @param general_date
 * @returns {Promise<[{election_date, end_date: *, reporting_period_type: string, due_date: *, report_type: string, start_date: string},{election_date, end_date: *, reporting_period_type: string, due_date: *, report_type: string, start_date: *},{election_date, end_date: *, reporting_period_type: string, due_date: *, report_type: string, start_date: *},{election_date, end_date: *, reporting_period_type: string, due_date: *, report_type: string, start_date: string},{election_date, end_date: string, reporting_period_type: string, due_date: *, report_type: string, start_date: *},null,null,null,null,null,null,null]>}
 */
async function makeReportPeriodsDates(february_date, april_date, primary_date, general_date) {
    // let dueDate = new Date(year, month + 1, 15);
    // dueDate = adjustForWeekendsAndHolidaysL2(dueDate);
    // const dueDateFormat = dueDate.toISOString().split('T')[0];
    return [
        {
            "election_date": february_date,
            "start_date": february_date.split('-')[0] - 1 + '-12-01',
            "end_date": calculateDaysBefore(new Date(february_date), 22),
            "due_date": calculateDaysBefore(new Date(february_date), 21),
            "report_type": "C4",
            "reporting_period_type": "TwentyOneDayPreElection",
            "reminder_date": null,
        },
        {
            "election_date": february_date,
            "start_date": calculateDaysBefore((new Date(february_date)), 7),
            "end_date": new Date(new Date(february_date).getFullYear(), 2, 0).toISOString().split('T')[0], // last day of february
            "due_date": calculateDaysAfter(new Date(february_date), 27, "leapYearCheck"),
            "report_type": "C4",
            "reporting_period_type": "PostElection",
            "reminder_date": null,
        },
        {
            "election_date": february_date,
            "start_date": calculateDaysBefore(new Date(february_date), 21),
            "end_date": calculateDaysBefore(new Date(february_date), 8),
            "due_date": calculateDaysBefore(new Date(february_date), 7),
            "report_type": "C4",
            "reporting_period_type": "SevenDayPreElection",
            "reminder_date": null,
        },
        {
            "election_date": april_date,
            "start_date": calculateDaysBefore(new Date(april_date), 21),
            "end_date": calculateDaysBefore(new Date(april_date), 8),
            "due_date": calculateDaysBefore(new Date(april_date), 7),
            "report_type": "C4",
            "reporting_period_type": "SevenDayPreElection",
            "reminder_date": null,
        },
        {
            "election_date": april_date,
            "start_date": calculateDaysBefore(new Date(april_date), 53),
            "end_date": calculateDaysBefore(new Date(april_date), 22),
            "due_date": calculateDaysBefore(new Date(april_date), 21),
            "report_type": "C4",
            "reporting_period_type": "TwentyOneDayPreElection",
            "reminder_date": null,
        },
        {
            "election_date": april_date,
            "start_date": calculateDaysBefore(new Date(april_date), 7),
            "end_date": calculateDaysAfter(new Date(april_date), 7),
            "due_date": calculateDaysAfter(new Date(april_date), 17),
            "report_type": "C4",
            "reporting_period_type": "PostElection",
            "reminder_date": null,
        },
        {
            "election_date": primary_date,
            "start_date": calculateDaysBefore(new Date(primary_date), 66),
            "end_date": calculateDaysBefore(new Date(primary_date), 22),
            "due_date": calculateDaysBefore(new Date(primary_date), 21),
            "report_type": "C4",
            "reporting_period_type": "TwentyOneDayPrePrimary",
            "reminder_date": null,
        },
        {
            "election_date": primary_date,
            "start_date": calculateDaysBefore(new Date(primary_date), 7,),
            "end_date": calculateDaysAfter(new Date(primary_date), 25,),
            "due_date": calculateDaysAfter(new Date(primary_date), 35,),
            "report_type": "C4",
            "reporting_period_type": "PostPrimary",
            "reminder_date": null,
        },
        {
            "election_date": primary_date,
            "start_date": calculateDaysBefore(new Date(primary_date), 21),
            "end_date": calculateDaysBefore(new Date(primary_date), 8),
            "due_date": calculateDaysBefore(new Date(primary_date), 7),
            "report_type": "C4",
            "reporting_period_type": "SevenDayPrePrimary",
            "reminder_date": null,
        },
        {
            "election_date": general_date,
            "start_date": calculateDaysBefore(new Date(general_date), 20),
            "end_date": calculateDaysBefore(new Date(general_date), 7),
            "due_date": calculateDaysBefore(new Date(general_date), 6),
            "report_type": "C4",
            "reporting_period_type": "SevenDayPreGeneral",
            "reminder_date": null,
        },
        {
            "election_date": general_date,
            "start_date": calculateDaysBefore(new Date(general_date), 64),
            "end_date": calculateDaysBefore(new Date(general_date), 21 ),
            "due_date": calculateDaysBefore(new Date(general_date), 20 ),
            "report_type": "C4",
            "reporting_period_type": "TwentyOneDayPreGeneral",
            "reminder_date": null,
        },
        {
            "election_date": general_date,
            "start_date": calculateDaysBefore(new Date(general_date), 7),
            "end_date": calculateDaysAfter(new Date(general_date), 25),
            "due_date": calculateDaysAfter(new Date(general_date), 35),
            "report_type": "C4",
            "reporting_period_type": "PostGeneral",
            "reminder_date": null,
        },
        {
            "reporting_period_type": "Monthly",
            "start_date": february_date.split('-')[0] + "-01-01",
            "end_date": february_date.split('-')[0] +"-01-31",
            "due_date": february_date.split('-')[0] + "-02-10",
            "report_type": "C4",
            "election_date": null,
            "reminder_date": null,
        },
        {
            "reporting_period_type": "Monthly",
            "start_date": february_date.split('-')[0] + "-02-01",
            "end_date": february_date.split('-')[0] +"-02-" + getLastDayOfFebruary(february_date.split('-')[0]),
            "due_date": february_date.split('-')[0] + "-03-10",
            "report_type": "C4",
            "election_date": null,
            "reminder_date": null,
        },
        {
            "reporting_period_type": "Monthly",
            "start_date": february_date.split('-')[0] + "-03-01",
            "end_date": february_date.split('-')[0] + "-03-31",
            "due_date": february_date.split('-')[0] + "-04-10",
            "report_type": "C4",
            "election_date": null,
            "reminder_date": null,
        },
        {
            "reporting_period_type": "Monthly",
            "start_date": february_date.split('-')[0] + "-04-01",
            "end_date": february_date.split('-')[0] + "-04-30",
            "due_date": february_date.split('-')[0] + "-05-10",
            "report_type": "C4",
            "election_date": null,
            "reminder_date": null,
        },
        {
            "reporting_period_type": "Monthly",
            "start_date": february_date.split('-')[0] + "-05-01",
            "end_date": february_date.split('-')[0] + "-05-31",
            "due_date": february_date.split('-')[0] + "-06-10",
            "report_type": "C4",
            "election_date": null,
            "reminder_date": null,
        },
        {
            "reporting_period_type": "Monthly",
            "start_date": february_date.split('-')[0] + "-06-01",
            "end_date": february_date.split('-')[0] + "-06-30",
            "due_date": february_date.split('-')[0] + "-07-10",
            "report_type": "C4",
            "election_date": null,
            "reminder_date": null,
        },
        {
            "reporting_period_type": "Monthly",
            "start_date": february_date.split('-')[0] + "-07-01",
            "end_date": february_date.split('-')[0] + "-07-31",
            "due_date": february_date.split('-')[0] + "-08-10",
            "report_type": "C4",
            "election_date": null,
            "reminder_date": null,
        },
        {
            "reporting_period_type": "Monthly",
            "start_date": february_date.split('-')[0] + "-08-01",
            "end_date": february_date.split('-')[0] + "-08-31",
            "due_date": february_date.split('-')[0] + "-09-10",
            "report_type": "C4",
            "election_date": null,
            "reminder_date": null,
        },
        {
            "reporting_period_type": "Monthly",
            "start_date": february_date.split('-')[0] + "-09-01",
            "end_date": february_date.split('-')[0] + "-09-30",
            "due_date": february_date.split('-')[0] + "-10-10",
            "report_type": "C4",
            "election_date": null,
            "reminder_date": null,
        },
        {
            "reporting_period_type": "Monthly",
            "start_date": february_date.split('-')[0] + "-10-01",
            "end_date": february_date.split('-')[0] + "-10-31",
            "due_date": february_date.split('-')[0] + "-11-10",
            "report_type": "C4",
            "election_date": null,
            "reminder_date": null,
        },
        {
            "reporting_period_type": "Monthly",
            "start_date": february_date.split('-')[0] + "-11-01",
            "end_date": february_date.split('-')[0] + "-11-30",
            "due_date": february_date.split('-')[0] + "-12-10",
            "report_type": "C4",
            "election_date": null,
            "reminder_date": null,
        },
        {
            "reporting_period_type": "Monthly",
            "start_date": february_date.split('-')[0] + "-12-01",
            "end_date": february_date.split('-')[0] + "-12-31",
            "due_date": await changeTheYear(february_date, 1,"01-10"),
            "report_type": "C4",
            "election_date": null,
            "reminder_date": null,
        }
    ]
}

async function changeTheYear(dateStr, yearIncrementBy, concatStr){
    const date = new Date(dateStr);
    date.setFullYear(date.getFullYear() + yearIncrementBy);
    const year = date.getFullYear();

    return  `${year}-${concatStr}`
}

/**
 * Generate the election dates for the C4 report periods
 * @param february_date
 * @param april_date
 * @param primary_date
 * @param general_date
 */
async function makeElectionDates(february_date, april_date, primary_date, general_date) {
    return [
        {
            "election_code": february_date.split('-')[0] + "S2",
            "title": february_date.split('-')[0] + " February Special",
            "is_primary": false,
            "is_special": true,
            "election_date": february_date
        },
        {
            "election_code": april_date.split('-')[0] + "S4",
            "title": april_date.split('-')[0] + " April Special",
            "is_primary": false,
            "is_special": true,
            "election_date": april_date
        },
        {
            "election_code": primary_date.split('-')[0] + "P",
            "title": primary_date.split('-')[0] + " Primary",
            "is_primary": true,
            "is_special": false,
            "election_date": primary_date
        }, {
            "election_code": general_date.split('-')[0] + "G",
            "title": general_date.split('-')[0] + " General",
            "is_primary": false,
            "is_special": false,
            "election_date": general_date
        }
    ];
}

/**
 * Generate the report periods for the l2 & l3 lobbyist reports
 * @param start_year
 * @param end_year
 * @param report_type
 * @returns {Promise<*[]>}
 */
export async function generateLobbyistReportPeriodDates(start_year, end_year, report_type) {
    start_year = parseInt(start_year);
    end_year = parseInt(end_year);
    if (report_type === 'L3') {
        return generateL3ReportPeriods(start_year, end_year, report_type);
    } else if (report_type === 'L2') {
        return generateL2ReportPeriods(start_year, end_year, report_type);
    }
}

/**
 * Generate the C4 report periods
 * @param february_date
 * @param april_date
 * @param primary_date
 * @param general_date
 * @returns {Promise<{periods: {general: [{election_date, end_date: string, reporting_period_type: string, due_date: string, report_type: string, start_date: string},{election_date, end_date: string, reporting_period_type: string, due_date: string, report_type: string, start_date: string},{election_date, end_date: string, reporting_period_type: string, due_date: string, report_type: string, start_date: string}], february: [{election_date, end_date: string, reporting_period_type: string, due_date: string, report_type: string, start_date: string},{election_date, end_date: string, reporting_period_type: string, due_date: string, report_type: string, start_date: string},{election_date, end_date: string, reporting_period_type: string, due_date: string, report_type: string, start_date: string}], april: [{election_date, end_date: string, reporting_period_type: string, due_date: string, report_type: string, start_date: string},{election_date, end_date: string, reporting_period_type: string, due_date: string, report_type: string, start_date: string},{election_date, end_date: string, reporting_period_type: string, due_date: string, report_type: string, start_date: string}], primary: [{election_date, end_date: string, reporting_period_type: string, due_date: string, report_type: string, start_date: string},{election_date, end_date: string, reporting_period_type: string, due_date: string, report_type: string, start_date: string},{election_date, end_date: string, reporting_period_type: string, due_date: string, report_type: string, start_date: string}]}, elections: {february_special: {election_date, is_special: boolean, is_primary: boolean, election_code: string, title: string}, primary_election: {election_date, is_special: boolean, is_primary: boolean, election_code: string, title: string}, april_special: {election_date, is_special: boolean, is_primary: boolean, election_code: string, title: string}, general_election: {election_date, is_special: boolean, is_primary: boolean, election_code: string, title: string}}}>}
 */
export async function generateC4ReportPeriods(february_date, april_date, primary_date, general_date) {
    if (february_date.length < 0) {
        throw new Error('February date is required');
    }
    if (april_date.length < 0) {
        throw new Error('April date is required');
    }
    if (primary_date.length < 0) {
        throw new Error('Primary date is required');
    }
    if (general_date.length < 0) {
        throw new Error('General date is required');
    }
    const elections = await makeElectionDates(february_date, april_date, primary_date, general_date);
    const periods = await makeReportPeriodsDates(february_date, april_date, primary_date, general_date);
    return {elections, periods};
}
