export const reporting_periods = () => {
    return [
        {
            "reporting_period_id": 704,
            "reporting_period_type": "Monthly",
            "election_date": null,
            "start_date": "2023-12-01",
            "end_date": "2023-12-31",
            "due_date": "2024-01-16",
            "report_type": "L2",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 705,
            "reporting_period_type": "Monthly",
            "election_date": null,
            "start_date": "2024-01-01",
            "end_date": "2024-01-31",
            "due_date": "2024-02-15",
            "report_type": "L2",
            "reminder_date": "2024-01-01",
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 706,
            "reporting_period_type": "Monthly",
            "election_date": null,
            "start_date": "2024-02-01",
            "end_date": "2024-02-29",
            "due_date": "2024-03-15",
            "report_type": "L2",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 707,
            "reporting_period_type": "Monthly",
            "election_date": null,
            "start_date": "2024-03-01",
            "end_date": "2024-03-31",
            "due_date": "2024-04-15",
            "report_type": "L2",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 708,
            "reporting_period_type": "Monthly",
            "election_date": null,
            "start_date": "2024-04-01",
            "end_date": "2024-04-30",
            "due_date": "2024-05-15",
            "report_type": "L2",
            "reminder_date": "2024-04-01",
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 709,
            "reporting_period_type": "Monthly",
            "election_date": null,
            "start_date": "2024-05-01",
            "end_date": "2024-05-31",
            "due_date": "2024-06-17",
            "report_type": "L2",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 710,
            "reporting_period_type": "Monthly",
            "election_date": null,
            "start_date": "2024-06-01",
            "end_date": "2024-06-30",
            "due_date": "2024-07-15",
            "report_type": "L2",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 711,
            "reporting_period_type": "Monthly",
            "election_date": null,
            "start_date": "2024-07-01",
            "end_date": "2024-07-31",
            "due_date": "2024-08-15",
            "report_type": "L2",
            "reminder_date": "2024-07-01",
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 712,
            "reporting_period_type": "Monthly",
            "election_date": null,
            "start_date": "2024-08-01",
            "end_date": "2024-08-31",
            "due_date": "2024-09-16",
            "report_type": "L2",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 713,
            "reporting_period_type": "Monthly",
            "election_date": null,
            "start_date": "2024-09-01",
            "end_date": "2024-09-30",
            "due_date": "2024-10-15",
            "report_type": "L2",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 714,
            "reporting_period_type": "Monthly",
            "election_date": null,
            "start_date": "2024-10-01",
            "end_date": "2024-10-31",
            "due_date": "2024-11-15",
            "report_type": "L2",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 715,
            "reporting_period_type": "Monthly",
            "election_date": null,
            "start_date": "2024-11-01",
            "end_date": "2024-11-30",
            "due_date": "2024-12-16",
            "report_type": "L2",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 807,
            "reporting_period_type": "Annual",
            "election_date": null,
            "start_date": "2023-01-01",
            "end_date": "2023-12-31",
            "due_date": "2024-02-12",
            "report_type": "L3",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 181,
            "reporting_period_type": "TwentyOneDayPreElection",
            "election_date": "2024-02-13",
            "start_date": "2023-12-01",
            "end_date": "2024-01-22",
            "due_date": "2024-01-23",
            "report_type": "C4",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 183,
            "reporting_period_type": "PostElection",
            "election_date": "2024-02-13",
            "start_date": "2024-02-06",
            "end_date": "2024-02-29",
            "due_date": "2024-03-11",
            "report_type": "C4",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 182,
            "reporting_period_type": "SevenDayPreElection",
            "election_date": "2024-02-13",
            "start_date": "2024-01-23",
            "end_date": "2024-02-05",
            "due_date": "2024-02-06",
            "report_type": "C4",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 185,
            "reporting_period_type": "SevenDayPreElection",
            "election_date": "2024-04-23",
            "start_date": "2024-04-02",
            "end_date": "2024-04-15",
            "due_date": "2024-04-16",
            "report_type": "C4",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 184,
            "reporting_period_type": "TwentyOneDayPreElection",
            "election_date": "2024-04-23",
            "start_date": "2024-03-01",
            "end_date": "2024-04-01",
            "due_date": "2024-04-02",
            "report_type": "C4",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 186,
            "reporting_period_type": "PostElection",
            "election_date": "2024-04-23",
            "start_date": "2024-04-16",
            "end_date": "2024-04-30",
            "due_date": "2024-05-10",
            "report_type": "C4",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 175,
            "reporting_period_type": "TwentyOneDayPrePrimary",
            "election_date": "2024-08-06",
            "start_date": "2024-06-01",
            "end_date": "2024-07-15",
            "due_date": "2024-07-16",
            "report_type": "C4",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 177,
            "reporting_period_type": "PostPrimary",
            "election_date": "2024-08-06",
            "start_date": "2024-07-30",
            "end_date": "2024-08-31",
            "due_date": "2024-09-10",
            "report_type": "C4",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 176,
            "reporting_period_type": "SevenDayPrePrimary",
            "election_date": "2024-08-06",
            "start_date": "2024-07-16",
            "end_date": "2024-07-29",
            "due_date": "2024-07-30",
            "report_type": "C4",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 179,
            "reporting_period_type": "SevenDayPreGeneral",
            "election_date": "2024-11-05",
            "start_date": "2024-10-15",
            "end_date": "2024-10-28",
            "due_date": "2024-10-29",
            "report_type": "C4",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 178,
            "reporting_period_type": "TwentyOneDayPreGeneral",
            "election_date": "2024-11-05",
            "start_date": "2024-09-01",
            "end_date": "2024-10-14",
            "due_date": "2024-10-15",
            "report_type": "C4",
            "reminder_date": null,
            "reminder_sent_date": null
        },
        {
            "reporting_period_id": 180,
            "reporting_period_type": "PostGeneral",
            "election_date": "2024-11-05",
            "start_date": "2024-10-29",
            "end_date": "2024-11-30",
            "due_date": "2024-12-10",
            "report_type": "C4",
            "reminder_date": null,
            "reminder_sent_date": null
        }
    ]
}