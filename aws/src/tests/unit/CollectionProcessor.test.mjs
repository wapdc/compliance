import {test, describe, beforeAll, afterAll, expect, jest} from "@jest/globals";
import env from 'dotenv'
import * as rds from '@wapdc/common/wapdc-db'
import {CollectionProcessor} from "../../CollectionProcessor.mjs";
import path from 'path'
import * as url from 'url';
const __dirname = url.fileURLToPath(new URL('.', import.meta.url));
const collectionDir = path.join(__dirname, '..', '..', 'tests','data');
import sgMail from "@sendgrid/mail"
import moment from "moment";

env.config()

beforeAll(async () => {
  await rds.connect();
  rds.beginTransaction();
})
afterAll(async () => {
  await rds.rollback()
  await rds.close()
})
describe('Collection creation',  () => {
  const cp = new CollectionProcessor(collectionDir);
  let collectionId

  // Verify that we can create a collection from a file definition in the database
  test('Can generate collection', async () => {
    collectionId = await cp.createCollection('digits', {key: 'test-foo-1'});

    const cRow = await rds.fetch("select * from collection where collection_id = $1", [collectionId])
    expect(cRow.definition).toBe('digits.yml')
    expect(cRow.collection_key).toBe('test-foo-1')
  })

  test('can populate simple collection', async () => {
    // uses last first collection created
    collectionId = await cp.createCollection('digits');
    await cp.updateCollectionMembers()

    // Check to ensure that collection members got created
    const members = await rds.query(`select * from collection_member where collection_id = $1 order by member_id`, [collectionId])

    expect(members).not.toBeNull();
    expect(members.length).toBe(10)

    const first = members[0]
    expect(first.target_id).toBe(0)
    expect(first.compare_id).toBe(0)
    expect(first.email).toBe('noreply@example.com')
    expect(first.address).toBe('121 elm st nw')
    expect(first.city).toBe('Olympia')
    expect(first.state).toBe('WA')
    expect(first.postcode).toBe('98504')
    expect(first.phone).toBe('360-587-9658')
  })

  test('Can add members to the collection', async () => {
    // Start with a populated collection
    await cp.updateCollectionMembers(true)

    // Delete one of the entires
    await rds.execute("DELETE from collection_member WHERE collection_id = $1", [collectionId])

    // Repopulate and make sure we have the right number of rows
    await cp.updateCollectionMembers()
    {
      const {count} = await rds.fetch(`SELECT COUNT(1) as count from collection_member WHERE collection_id = $1`,[collectionId])
      expect(count).toBe("10")
    }

    // Add an extra item manually
    await rds.execute(`INSERT INTO collection_member(collection_id, target_id, label,excluded) 
                 VALUES ($1, '11', '11', false)`, [collectionId])

    // Call update with reset set to make sure we remove the manually added one
    await cp.updateCollectionMembers(true)
    {
      const {count} = await rds.fetch(`SELECT COUNT(1) as count from collection_member WHERE collection_id = $1`,[collectionId])
      expect(count).toEqual("10")
    }

    // Make sure first member is correct
    {
      const member = await rds.fetch(`SELECT * FROM collection_member where collection_id=$1 and target_id='1'`, [collectionId])
      expect(member.email).toBe('noreply@example.com')
      expect(member.label).toBe('1')
      expect(member.compare_id).toBe(1)
      expect(parseFloat(member.score)).toBe(1.0)
    }

  })

  test('Can peform execute operation', async () => {
    await cp.performAction('populate', true)
    await cp.performAction('delete_sevens')

    // Make sure the query had executed.
    const member = await rds.fetch(`SELECT * from collection_member where  target_id='7' and collection_id = $1 `, [collectionId])
    expect(member).toBeNull()
  })

  test('Can update exclusion information', async() => {
    collectionId = await cp.createCollection('exclude1-4')
    await cp.updateCollectionMembers()
    {
      const {count} = await rds.fetch(`SELECT count(1)::integer as count from collection_member where collection_id=$1 and excluded=false`,[collectionId])
      expect(count).toBe(6)
    }
    {
      const member = await rds.fetch(`SELECT * from collection_member where collection_id=$1 and target_id=1`, [collectionId])
      expect(member.excluded).toBe(true)
      expect(member.flag).toBe('excluded')
      expect(member.metadata).toBe("1")
    }
  })

  test('Can execute queries with parameters', async() => {
    // Try with the defaults
    collectionId = await cp.createCollection('parameter')
    await cp.updateCollectionMembers()
    {
      const {count} = await rds.fetch(`SELECT count(1)::int from collection_member where collection_id=$1 and excluded=false`, [collectionId])
      expect(count).toBe(10)
    }
    // Try again overriding the defaults
    collectionId = await cp.createCollection('parameter', {metadata: {a_number: 5}})
    await cp.updateCollectionMembers()
    {
      const {count} = await rds.fetch(`SELECT count(1)::int from collection_member where collection_id=$1 and excluded=false`, [collectionId])
      expect(count).toBe(5)
    }
  })

  test('Can retrieve a collection', async () => {
    collectionId = await cp.createCollection('digits', {key: 'test-collection-3', title: 'daves collection', metadata: { test: 'test description'}})
    const collection = await cp.getCollection(collectionId)
    expect(collection.collection_id).toBe(collectionId)
    expect(collection.key).toBe('test-collection-3')
    expect(collection.metadata.toBeDefined)
    expect(collection.metadata.test).toBe('test description')
    expect(collection.title).toBe('daves collection')
    expect(collection.updated_at).toBeDefined()
  })

  test('Can remove a collection',async () => {
    collectionId = await cp.createCollection('digits')
    await cp.updateCollectionMembers()
    await cp.removeCollection(collectionId)
    const collection = await rds.fetch(`select * from collection where collection_id =$1`, [collectionId])
    expect(collection).toBe(null)
    const {count} = await rds.fetch(`select count(1)::int from collection_member where collection_id=$1`, [collectionId] )
    expect(count).toBe(0)
  })

  test('Can update a definition of a collection', async() => {
    collectionId = await cp.createCollection('digits')
    const collection = await cp.getCollection(collectionId);
    // Modify some porperties
    collection.title = "Changed title"
    collection.template = "exclude1-4.yml"
    await cp.updateDefinition(collection)
    const modifiedCollection = await cp.getCollection(collectionId)
    expect(modifiedCollection.title).toBe("Changed title")
    expect(modifiedCollection.template).toBe("exclude1-4.yml")
  })

  test('Can find a unique collection', async() => {
    collectionId = await cp.createCollection('digits', {key: 'test-digits-1'})
    const collection = await cp.getUniqueCollection('numbers', 'test', 'test-digits-1')
    expect(collection).toBeDefined()
    expect(collection).not.toBeNull()
  })

  test('Can update member info', async() => {
    collectionId = await cp.createCollection('digits')
    await cp.updateCollectionMembers()

    const member = {
      target_id: 7,
      compare_id: 8,
      collection_id: collectionId,
      unknown_attribute: "foo",
      excluded: true,
      flag: "Flagged",
      memo: "An updated memo",
      office_code:"32",
      jurisdiction_id: 4150,
      metadata: {
        saving: 'yes'
      }
    }

    await cp.updateMemberInfo(member)

    const mm = await rds.fetch(`SELECT collection_id, compare_id, excluded, flag, memo, office_code, jurisdiction_id, CAST(metadata as JSON)FROM collection_member where target_id=7 and collection_id=$1`, [collectionId])
    expect(mm.flag).toBe(member.flag)
    expect(mm.compare_id).toBe(member.compare_id)
    expect(mm.excluded).toBe(member.excluded)
    expect(mm.memo).toBe(member.memo)
    expect(mm.office_code).toBe(member.office_code)
    expect(mm.jurisdiction_id).toBe(member.jurisdiction_id)
    expect(mm.metadata).toBeDefined()
    expect(mm.metadata).not.toBeNull()
    expect(mm.metadata.saving).toBe('yes')
  })

  test('Can send emails to collections', async() => {
    // Mock implementation of sgMail from sendgrid
    jest.mock('@sendgrid/mail')
    let lastMessage
    sgMail.send = jest.fn().mockImplementation((message) => {
      lastMessage = message;
    })

    collectionId = await cp.createCollection('exclude1-4')
    await cp.updateCollectionMembers()
    const {ids} = await rds.fetch(`select array_agg(member_id) as ids from collection_member where collection_id=$1 and target_id in (8,9)`, [collectionId] )

    const template='t-12j4h569ch390b763hj'
    // Test with the collection id

    await cp.sendEmails(collectionId,template)
    // Only non-excluded members should have been updated
    {
      const {count} = await rds.fetch(`SELECT count(1)::int from collection_member where collection_id=$1 and last_contacted is not null`, [collectionId])
      expect(count).toBe(6)
    }
    // Check to make sure we sent the right number of emails.
    expect(lastMessage.personalizations.length).toBe(6)
    expect(lastMessage.personalizations[0].to).toEqual(['test@noreply.com', 'test2@noreply.com'])


    // Clear out last_contacted list

    await rds.execute(`UPDATE collection_member set last_contacted=null where collection_id=$1`,[collectionId])
    // FInd out the last two ids of the collection

    await cp.sendEmails(collectionId, template, ids)
    {
      const {count} = await rds.fetch(`SELECT count(1)::int from collection_member where collection_id=$1 and last_contacted is not null`, [collectionId])
      expect(count).toBe(2)
    }
    // Check to make sure we sent the right number of emails
    expect(lastMessage.personalizations.length).toBe(2)

  })

})

describe('Copy collection', () => {
  const cp = new CollectionProcessor(collectionDir);
  let originalCollectionId
  test('can copy collection', async() => {
    originalCollectionId = await cp.createCollection('digits');
    await cp.updateCollectionMembers()

    let newCollectionId = await cp.copyCollection(originalCollectionId, 'Test title');
    const collection = await rds.fetch('select * from collection where collection_id = $1', [newCollectionId])
    expect(collection).not.toBeNull();
    expect(collection.collection_group).toBe('test')
    expect(collection.target).toBe('numbers')
    expect(collection.category).toBe('test')
    expect(collection.definition).toBe('digits.yml')
    expect(collection.metadata).toBe("{\"language\":\"english\"}")

    const members = await rds.fetch(`select * from collection_member where collection_id = $1 order by member_id limit 1`, [newCollectionId])
    expect(members.target_id).toBe(0)
    expect(members.compare_id).toBe(0)
    expect(members.excluded).toBe(false)
    expect(members.email).toBe('noreply@example.com')
    expect(members.score).toBe('0.00')
  })
})
describe('Can exclude/include members into collection',()=> {
  const cp = new CollectionProcessor(collectionDir);
  let collectionId
  test('Updates memo field when memo is provided', async() => {
    collectionId = await cp.createCollection('digits')
    await cp.updateCollectionMembers()
    const payload = {
      ids: [7,1,2],
      excluded: true,
      bulkMemoText: "new memo"
    };
    await cp.excludeMembers(collectionId, payload);
    const query = `SELECT target_id, excluded, memo FROM collection_member WHERE collection_id = $1 and target_id = ANY($2)`;
    for (const id of payload.ids) {
      const memberData = await rds.fetch(query, [collectionId, [id]]);
      expect(memberData).toBeDefined();
      expect(memberData.target_id).toBe(id);
      expect(memberData.excluded).toBe(payload.excluded);
      expect(memberData.memo).toBe(payload.bulkMemoText);
    }
  });
  test('Leaves memo field unchanged when memo is empty', async () => {
    collectionId = await cp.createCollection('digits')
    await cp.updateCollectionMembers()
    const originalMemo = "original memo"
    const payload = {
      ids: [1, 2 , 3],
      excluded: true,
      bulkMemoText : ''
    }
    // Set up initial state with original memo
    await cp.excludeMembers(collectionId, { ids: payload.ids, excluded: true, bulkMemoText: originalMemo });
    // update memo with null/empty
    await cp.excludeMembers(collectionId, { ids: payload.ids, excluded: payload.excluded, bulkMemoText: payload.bulkMemoText });
    for (const id of payload.ids) {
      const memberData = await rds.fetch(`SELECT target_id, excluded,memo FROM collection_member WHERE collection_id = $1 AND target_id = ANY($2)`, [collectionId, [id]]);
      expect(memberData).toBeDefined();
      expect(memberData.target_id).toBe(id);
      expect(memberData.excluded).toBe(payload.excluded);
      expect(memberData.memo).toBe(originalMemo);
    }
  });
})

describe("Create report_period reminders", () => {
  const cp = new CollectionProcessor(collectionDir);
  sgMail.send = jest.fn()

  test('Creates collection and sends emails', async () => {
    await rds.execute(
      `INSERT INTO reporting_period(report_type, reporting_period_type, start_date, end_date, due_date, reminder_date) 
         VALUES ('TEST','Monthly', '2024-01-01', '2024-01-31', '2024-02-15', now() - interval '1 DAY')`)
    //Sends report period period reminders for a report type based on collection.
    await cp.sendReportReminders('TEST',['Monthly'], 'parameter', (period) => {
      if (!period) {
        throw 'No Reporting period'
      }
      let start_date = new Date(moment(period.start_date))
      const month = start_date.getMonth() + 1
      return { exists: false, data: {group: 'test schedule', metadata: { a_number: month }, title: 'test schedule'} }
    })


    const collection = await rds.fetch(`
      SELECT collection_id, title, collection_group, definition, cast(metadata as JSON) as metadata from collection where collection_group='test schedule'`)

    // Make sure we created a collection
    expect(collection).toBeDefined()
    expect(collection.definition).toBe('parameter.yml')
    expect(collection.metadata.a_number).toBe(1)

    // Make sure that the collection has the right members
    const {members} = await rds.fetch(`select cast(count(*) as int) as members from collection_member where collection_id = $1`, [collection.collection_id])
    expect(members).toBe(1)

    // Make sure we updated the reporting period type
    const {reminder_sent_date} = await rds.fetch(`select reminder_sent_date from reporting_period where report_type='TEST'`)
    expect (reminder_sent_date).not.toBeNull()

    // Check to make sure we sent mail.
    expect(sgMail.send).toBeCalled()

  })
  test('Does not create collection twice', async () => {
    const {collections} =  await rds.fetch(`
      SELECT count(*)::int as collections from collection where collection_group='test schedule'`)
    expect(collections).toBe(1)
  })
})