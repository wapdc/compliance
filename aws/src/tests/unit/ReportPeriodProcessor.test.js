import {test, describe, beforeAll, afterAll, expect, jest} from "@jest/globals";
import env from 'dotenv'
import * as rds from '@wapdc/common/wapdc-db'
import { reporting_periods } from "../data/reporting_periods.mjs";
import {generateC4ReportPeriods, generateLobbyistReportPeriodDates} from "../../periodsGenerator.mjs"
import {ReportPeriodsProcessor} from "../../ReportPeriodsProcessor.mjs";

env.config()

beforeAll(async () => {
  await rds.connect();
  rds.beginTransaction();
})
afterAll(async () => {
  await rds.rollback()
  await rds.close()
})

describe("Create reporting periods and election dates", () => {
  const rp = new ReportPeriodsProcessor();
  const payload = reporting_periods();

  test('Tests ', async () => {
    expect(payload).not.toBeNull()
    const c4 = payload.find((p) => p.report_type === 'C4')
    const l2 = payload.find((p) => p.report_type === 'L2')
    const l3 = payload.find((p) => p.report_type === 'L3')
    expect(c4).toBeDefined()
    expect(l2).toBeDefined()
    expect(l3).toBeDefined()
  })

  test('Can update the due date', async () => {
    const l2 = payload.find((p) => p.reporting_period_id === 712 && p.report_type === 'L2')
    const due_date = '2024-09-15'
    const newL2Data = {
      "reporting_period_id": l2.reporting_period_id,
      "reporting_period_type": l2.reporting_period_type,
      "election_date": null,
      "start_date": l2.start_date,
      "end_date": l2.end_date,
      "due_date": due_date,
      "report_type": "L2",
      "reminder_date": l2.reminder_date,
      "reminder_sent_date": null
    }

    await rp.updateReportingPeriod(l2.reporting_period_id, newL2Data)
    const record  = await rds.fetch(`select * from wapdc.public.reporting_period where reporting_period_id = $1`, [l2.reporting_period_id])
    expect(record.due_date).toBe(due_date)
  })

  test('Can update L3 due date', async () => {
    const l3 = payload.find((p) => p.reporting_period_id === 807 && p.report_type === 'L3')
    const due_date = '2024-02-03'
    const newL3Data = {
      "reporting_period_id": l3.reporting_period_id,
      "reporting_period_type": l3.reporting_period_type,
      "election_date": null,
      "start_date": l3.start_date,
      "end_date": l3.end_date,
      "due_date": due_date,
      "report_type": "L3",
      "reminder_date": l3.reminder_date,
      "reminder_sent_date": null
    }
    await rp.updateReportingPeriod(l3.reporting_period_id, newL3Data)
    const record  = await rds.fetch(`select * from wapdc.public.reporting_period where reporting_period_id = $1`, [l3.reporting_period_id])
    expect(record.due_date).toBe(due_date)
  })

  test('Can update C4 due date', async () => {
    const c4 = payload.find((p) => p.reporting_period_id === 177 && p.report_type === 'C4')
    const due_date = '2024-09-06'
    const newC4Data = {
      "reporting_period_id": c4.reporting_period_id,
      "reporting_period_type": c4.reporting_period_type,
      "election_date": null,
      "start_date": c4.start_date,
      "end_date": c4.end_date,
      "due_date": due_date,
      "report_type": "C4",
      "reminder_date": c4.reminder_date,
      "reminder_sent_date": null
    }
    await rp.updateReportingPeriod(c4.reporting_period_id, newC4Data)
    const record  = await rds.fetch(`select * from reporting_period where reporting_period_id = $1`, [c4.reporting_period_id])
    expect(record.due_date).toBe(due_date)
  })

  test('Can save L2 generated reporting periods', async () => {
    const data = await generateLobbyistReportPeriodDates(2099, 2099, 'L2')
    expect(data.length).toBe(12)
    const october = data.filter((l2) => l2.start_date.includes('2099-10'))[0]
    let start_date = october.start_date
    expect(october.reporting_period_type).toBe('Monthly')
    expect(october.start_date).toBe('2099-10-01')
    expect(october.end_date).toBe('2099-10-31')
    expect(october.due_date).toBe('2099-11-16') // Note this can be altered by staff
    await rp.saveReportingPeriods(data)
    const newL2 = await rds.query(`select * from reporting_period where start_date = $1`, [start_date])
    expect(newL2.length).toBe(1)
  })

  test('Can save L3 generated reporting periods', async () => {
    let data = await generateLobbyistReportPeriodDates(2099, 2099, 'L3')
    expect(data.length).toBe(1)
    data = data[0]
    expect(data.reporting_period_type).toBe('Annual')
    expect(data.start_date).toBe('2099-01-01')
    expect(data.end_date).toBe('2099-12-31')
    expect(data.due_date).toBe('2099-03-02')  // Note this can be altered by staff - there is a test against the weekend
    await rp.saveReportingPeriods(data)
    const newL2 = await rds.query(`select *
                                   from reporting_period
                                   where start_date = $1`, [data.start_date])
    expect(newL2.length).toBe(2)
  })

  test('Can save C4 generated reporting periods', async () => {
    const february_date = '2099-02-13', april_date = '2099-04-23', primary_date = '2099-08-06', general_date = '2099-11-05'
    const { periods, elections} = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date)


    const reportingPeriods = periods.filter((p) => p.report_type === 'C4');
    await rp.saveReportingPeriods(reportingPeriods)
    const testData = await rds.query(`select reporting_period_type
                                        from reporting_period
                                        where report_type = 'C4' and to_char(election_date::date, 'yyyy') = $1`, [2099])
    expect(testData.length).toBe(12)
  })

  test('Can generate correct reporting periods for February special election', async () => {

    let february_date = '2099-02-13', april_date = '2099-04-23', primary_date = '2099-08-06', general_date = '2099-11-05'
    let { periods } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date)

    const c4Periods = periods.filter(p => p.report_type === 'C4');

    const twentyOneDayPreElection = c4Periods.find(p => p.reporting_period_type === 'TwentyOneDayPreElection' && p.election_date === february_date);
    expect(twentyOneDayPreElection).toBeDefined();
    expect(twentyOneDayPreElection.election_date).toBe(february_date)
    expect(twentyOneDayPreElection.start_date).toBe("2098-12-01")
    expect(twentyOneDayPreElection.end_date).toBe("2099-01-22")
    expect(twentyOneDayPreElection.due_date).toBe("2099-01-23")

    const postElection = c4Periods.find(p => p.reporting_period_type === 'PostElection' && p.election_date === february_date);
    expect(postElection).toBeDefined();
    expect(postElection.election_date).toBe(february_date)
    expect(postElection.start_date).toBe("2099-02-06")
    expect(postElection.end_date).toBe("2099-02-28")
    expect(postElection.due_date).toBe("2099-03-12")

    const sevenDayPreElection = c4Periods.find(p => p.reporting_period_type === 'SevenDayPreElection' && p.election_date === february_date);
    expect(sevenDayPreElection).toBeDefined();
    expect(sevenDayPreElection.election_date).toBe(february_date)
    expect(sevenDayPreElection.start_date).toBe("2099-01-23")
    expect(sevenDayPreElection.end_date).toBe("2099-02-05")
    expect(sevenDayPreElection.due_date).toBe("2099-02-06")
  })

  test('Can generate correct reporting periods for April special election', async () => {
    const february_date = '2099-02-13', april_date = '2099-04-23', primary_date = '2099-08-06', general_date = '2099-11-05'
    const { periods } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date)

    const c4Periods = periods.filter(p => p.report_type === 'C4');

    const AprilSpecial7dayPreElection = c4Periods.find((p) => p.reporting_period_type === 'SevenDayPreElection' && p.election_date === april_date)
    expect(AprilSpecial7dayPreElection).toBeDefined()
    expect(AprilSpecial7dayPreElection.election_date).toBe(april_date)
    expect(AprilSpecial7dayPreElection.start_date).toBe("2099-04-02")
    expect(AprilSpecial7dayPreElection.end_date).toBe("2099-04-15")
    expect(AprilSpecial7dayPreElection.due_date).toBe("2099-04-16")

    const AprilSpecialTwentyOneDayPreElection = c4Periods.find((p) => p.reporting_period_type === 'TwentyOneDayPreElection' && p.election_date === april_date)
    expect(AprilSpecialTwentyOneDayPreElection).toBeDefined()
    expect(AprilSpecialTwentyOneDayPreElection.election_date).toBe(april_date)
    expect(AprilSpecialTwentyOneDayPreElection.start_date).toBe("2099-03-01")
    expect(AprilSpecialTwentyOneDayPreElection.end_date).toBe("2099-04-01")
    expect(AprilSpecialTwentyOneDayPreElection.due_date).toBe("2099-04-02")

    const  AprilSpecialPostElection = c4Periods.find((p) => p.reporting_period_type === 'PostElection' && p.election_date === april_date)
    expect(AprilSpecialPostElection).toBeDefined()
    expect(AprilSpecialPostElection.election_date).toBe(april_date)
    expect(AprilSpecialPostElection.start_date).toBe("2099-04-16")
    expect(AprilSpecialPostElection.end_date).toBe("2099-04-30")
    expect(AprilSpecialPostElection.due_date).toBe("2099-05-10")
  })

  test('Can generate correct reporting periods for primary election', async () => {
    const february_date = '2099-02-13', april_date = '2099-04-23', primary_date = '2099-08-06', general_date = '2099-11-05'
    const { periods } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date)

    const c4Periods = periods.filter(p => p.report_type === 'C4');
    const TwentyOneDayPrePrimary = c4Periods.find((p) => p.reporting_period_type === 'TwentyOneDayPrePrimary' && p.report_type === 'C4')
    expect(TwentyOneDayPrePrimary).toBeDefined()
    expect(TwentyOneDayPrePrimary.election_date).toBe(primary_date)
    expect(TwentyOneDayPrePrimary.start_date).toBe("2099-06-01")
    expect(TwentyOneDayPrePrimary.end_date).toBe("2099-07-15")
    expect(TwentyOneDayPrePrimary.due_date).toBe("2099-07-16")

    const PostPrimary = c4Periods.find((p) => p.reporting_period_type === 'PostPrimary' && p.report_type === 'C4')
    expect(PostPrimary).toBeDefined()
    expect(PostPrimary.election_date).toBe(primary_date)
    expect(PostPrimary.start_date).toBe("2099-07-30")
    expect(PostPrimary.end_date).toBe("2099-08-31")
    expect(PostPrimary.due_date).toBe("2099-09-10")

    const SevenDayPrePrimary = c4Periods.find((p) => p.reporting_period_type === 'SevenDayPrePrimary' && p.report_type === 'C4')
    expect(SevenDayPrePrimary).toBeDefined()
    expect(SevenDayPrePrimary.election_date).toBe(primary_date)
    expect(SevenDayPrePrimary.start_date).toBe("2099-07-16")
    expect(SevenDayPrePrimary.end_date).toBe("2099-07-29")
    expect(SevenDayPrePrimary.due_date).toBe("2099-07-30")

  });

  test('Can generate correct reporting periods for general election', async () => {
    const february_date = '2099-02-13', april_date = '2099-04-23', primary_date = '2099-08-06', general_date = '2099-11-05'
    const { periods } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date)

    const c4Periods = periods.filter(p => p.report_type === 'C4');

    let SevenDayPreGeneral = c4Periods.find((p) => p.reporting_period_type === 'SevenDayPreGeneral' && p.report_type === 'C4')
    expect(SevenDayPreGeneral).toBeDefined()
    expect(SevenDayPreGeneral.election_date).toBe(general_date)
    expect(SevenDayPreGeneral.start_date).toBe("2099-10-15")
    expect(SevenDayPreGeneral.end_date).toBe("2099-10-28")
    expect(SevenDayPreGeneral.due_date).toBe("2099-10-29")

    let TwentyOneDayPreGeneral = c4Periods.find((p) => p.reporting_period_type === 'TwentyOneDayPreGeneral' && p.report_type === 'C4')
    expect(TwentyOneDayPreGeneral).toBeDefined()
    expect(TwentyOneDayPreGeneral.election_date).toBe(general_date)
    expect(TwentyOneDayPreGeneral.start_date).toBe("2099-09-01")
    expect(TwentyOneDayPreGeneral.end_date).toBe("2099-10-14")
    expect(TwentyOneDayPreGeneral.due_date).toBe("2099-10-15")

    let PostGeneral = c4Periods.find((p) => p.reporting_period_type === 'PostGeneral' && p.report_type === 'C4')
    expect(PostGeneral).toBeDefined()
    expect(PostGeneral.election_date).toBe(general_date)
    expect(PostGeneral.start_date).toBe("2099-10-28")
    expect(PostGeneral.end_date).toBe("2099-11-30")
    expect(PostGeneral.due_date).toBe("2099-12-10")
  });

  test('Can generate correct monthly C4 reporting periods for January', async () => {
    const february_date = '2099-02-13';
    const april_date = '2099-04-23';
    const primary_date = '2099-08-06';
    const general_date = '2099-11-05';
    const { periods } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date);

    const c4MonthlyReport = periods.filter((p) => p.reporting_period_type === 'Monthly' && p.report_type === 'C4')
    const c4JanuaryMonthly = c4MonthlyReport.find(report => {
      return report.start_date.split('-')[1] === '01'
    })
    expect(c4JanuaryMonthly).toBeDefined()
    expect(c4JanuaryMonthly.start_date).toBe("2099-01-01")
    expect(c4JanuaryMonthly.end_date).toBe("2099-01-31")
    expect(c4JanuaryMonthly.due_date).toBe("2099-02-10")
  });

  test('Can generate correct monthly C4 reporting periods for February', async () => {
    const february_date = '2099-02-13';
    const april_date = '2099-04-23';
    const primary_date = '2099-08-06';
    const general_date = '2099-11-05';
    const { periods } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date);
    const c4MonthlyReport = periods.filter((p) => p.reporting_period_type === 'Monthly' && p.report_type === 'C4')
    const c4FebruaryMonthly = c4MonthlyReport.find(report => {
      return report.start_date.split('-')[1] === '02'
    })
    expect(c4FebruaryMonthly).toBeDefined()
    expect(c4FebruaryMonthly.start_date).toBe("2099-02-01")
    expect(c4FebruaryMonthly.end_date).toBe("2099-02-28")
    expect(c4FebruaryMonthly.due_date).toBe("2099-03-10")
  });

  test('Can generate correct monthly C4 reporting periods for March', async () => {
    const february_date = '2099-02-13';
    const april_date = '2099-04-23';
    const primary_date = '2099-08-06';
    const general_date = '2099-11-05';
    const { periods } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date);
    const c4MonthlyReport = periods.filter((p) => p.reporting_period_type === 'Monthly' && p.report_type === 'C4')
    const c4MarchMonthly = c4MonthlyReport.find(report => {
      return report.start_date.split('-')[1] === '03'
    })
    expect(c4MarchMonthly).toBeDefined()
    expect(c4MarchMonthly.start_date).toBe("2099-03-01")
    expect(c4MarchMonthly.end_date).toBe("2099-03-31")
    expect(c4MarchMonthly.due_date).toBe("2099-04-10")
  });

  test('Can generate correct monthly C4 reporting periods for April', async () => {
    const february_date = '2099-02-13';
    const april_date = '2099-04-23';
    const primary_date = '2099-08-06';
    const general_date = '2099-11-05';
    const { periods } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date);
    const c4MonthlyReport = periods.filter((p) => p.reporting_period_type === 'Monthly' && p.report_type === 'C4')
    const c4AprilMonthly = c4MonthlyReport.find(report => {
      return report.start_date.split('-')[1] === '04'
    })
    expect(c4AprilMonthly).toBeDefined()
    expect(c4AprilMonthly.start_date).toBe("2099-04-01")
    expect(c4AprilMonthly.end_date).toBe("2099-04-30")
    expect(c4AprilMonthly.due_date).toBe("2099-05-10")
  });

  test('Can generate correct monthly C4 reporting periods for May', async () => {
    const february_date = '2099-02-13';
    const april_date = '2099-04-23';
    const primary_date = '2099-08-06';
    const general_date = '2099-11-05';
    const { periods } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date);

    const c4MonthlyReport = periods.filter((p) => p.reporting_period_type === 'Monthly' && p.report_type === 'C4')
    const c4MayMonthly = c4MonthlyReport.find(report => {
      return report.start_date.split('-')[1] === '05'
    })
    expect(c4MayMonthly).toBeDefined()
    expect(c4MayMonthly.start_date).toBe("2099-05-01")
    expect(c4MayMonthly.end_date).toBe("2099-05-31")
    expect(c4MayMonthly.due_date).toBe("2099-06-10")
  });

  test('Can generate correct monthly C4 reporting periods for June', async () => {
    const february_date = '2099-02-13';
    const april_date = '2099-04-23';
    const primary_date = '2099-08-06';
    const general_date = '2099-11-05';
    const { periods } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date);

    const c4MonthlyReport = periods.filter((p) => p.reporting_period_type === 'Monthly' && p.report_type === 'C4')
    const c4JuneMonthly = c4MonthlyReport.find(report => {
      return report.start_date.split('-')[1] === '06'
    })
    expect(c4JuneMonthly).toBeDefined()
    expect(c4JuneMonthly.start_date).toBe("2099-06-01")
    expect(c4JuneMonthly.end_date).toBe("2099-06-30")
    expect(c4JuneMonthly.due_date).toBe("2099-07-10")
  });

  test('Can generate correct monthly C4 reporting periods for July', async () => {
    const february_date = '2099-02-13';
    const april_date = '2099-04-23';
    const primary_date = '2099-08-06';
    const general_date = '2099-11-05';
    const { periods } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date);

    const c4MonthlyReport = periods.filter((p) => p.reporting_period_type === 'Monthly' && p.report_type === 'C4')
    const c4JulyMonthly = c4MonthlyReport.find(report => {
      return report.start_date.split('-')[1] === '07'
    })
    expect(c4JulyMonthly).toBeDefined()
    expect(c4JulyMonthly.start_date).toBe("2099-07-01")
    expect(c4JulyMonthly.end_date).toBe("2099-07-31")
    expect(c4JulyMonthly.due_date).toBe("2099-08-10")
  });

  test('Can generate correct monthly C4 reporting periods for August', async () => {
    const february_date = '2099-02-13';
    const april_date = '2099-04-23';
    const primary_date = '2099-08-06';
    const general_date = '2099-11-05';
    const { periods } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date);

    const c4MonthlyReport = periods.filter((p) => p.reporting_period_type === 'Monthly' && p.report_type === 'C4')
    const c4AugustMonthly = c4MonthlyReport.find(report => {
      return report.start_date.split('-')[1] === '08'
    })
    expect(c4AugustMonthly).toBeDefined()
    expect(c4AugustMonthly.start_date).toBe("2099-08-01")
    expect(c4AugustMonthly.end_date).toBe("2099-08-31")
    expect(c4AugustMonthly.due_date).toBe("2099-09-10")
  });

  test('Can generate correct monthly C4 reporting periods for September', async () => {
    const february_date = '2099-02-13';
    const april_date = '2099-04-23';
    const primary_date = '2099-08-06';
    const general_date = '2099-11-05';
    const { periods } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date);

    const c4MonthlyReport = periods.filter((p) => p.reporting_period_type === 'Monthly' && p.report_type === 'C4')
    const c4SeptemberMonthly = c4MonthlyReport.find(report => {
      return report.start_date.split('-')[1] === '09'
    })
    expect(c4SeptemberMonthly).toBeDefined()
    expect(c4SeptemberMonthly.start_date).toBe("2099-09-01")
    expect(c4SeptemberMonthly.end_date).toBe("2099-09-30")
    expect(c4SeptemberMonthly.due_date).toBe("2099-10-10")
  });

  test('Can generate correct monthly C4 reporting periods for October', async () => {
    const february_date = '2099-02-13';
    const april_date = '2099-04-23';
    const primary_date = '2099-08-06';
    const general_date = '2099-11-05';
    const { periods } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date);

    const c4MonthlyReport = periods.filter((p) => p.reporting_period_type === 'Monthly' && p.report_type === 'C4')
    const c4OctoberMonthly = c4MonthlyReport.find(report => {
      return report.start_date.split('-')[1] === '10'
    })
    expect(c4OctoberMonthly).toBeDefined()
    expect(c4OctoberMonthly.start_date).toBe("2099-10-01")
    expect(c4OctoberMonthly.end_date).toBe("2099-10-31")
    expect(c4OctoberMonthly.due_date).toBe("2099-11-10")
  });

  test('Can generate correct monthly C4 reporting periods for November', async () => {
    const february_date = '2099-02-13';
    const april_date = '2099-04-23';
    const primary_date = '2099-08-06';
    const general_date = '2099-11-05';
    const { periods } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date);

    const c4MonthlyReport = periods.filter((p) => p.reporting_period_type === 'Monthly' && p.report_type === 'C4')
    const c4NovemberMonthly = c4MonthlyReport.find(report => {
      return report.start_date.split('-')[1] === '11'
    })
    expect(c4NovemberMonthly).toBeDefined()
    expect(c4NovemberMonthly.start_date).toBe("2099-11-01")
    expect(c4NovemberMonthly.end_date).toBe("2099-11-30")
    expect(c4NovemberMonthly.due_date).toBe("2099-12-10")
  });

  test('Can generate correct monthly C4 reporting periods for December', async () => {
    const february_date = '2099-02-13';
    const april_date = '2099-04-23';
    const primary_date = '2099-08-06';
    const general_date = '2099-11-05';
    const { periods } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date);

    const c4MonthlyReport = periods.filter((p) => p.reporting_period_type === 'Monthly' && p.report_type === 'C4')
    const c4DecemberMonthly = c4MonthlyReport.find(report => {
      return report.start_date.split('-')[1] === '12'
    })
    expect(c4DecemberMonthly).toBeDefined()
    expect(c4DecemberMonthly.start_date).toBe("2099-12-01")
    expect(c4DecemberMonthly.end_date).toBe("2099-12-31")
    expect(c4DecemberMonthly.due_date).toBe("2100-01-10")
  });

  test('Can generate correct C4 election dates for the February Special', async () => {
    const february_date = '2099-02-13';
    const april_date = '2099-04-23';
    const primary_date = '2099-08-06';
    const general_date = '2099-11-05';
    const { elections } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date);
    const c4FebruarySpecialElection = elections.find((p) => p.report_type = 'C4' && p.title.includes('February Special') )
    expect(c4FebruarySpecialElection).toBeDefined()
    expect(c4FebruarySpecialElection.election_code).toBe('2099S2')
    expect(c4FebruarySpecialElection.title).toBe('2099 February Special')
    expect(c4FebruarySpecialElection.election_date).toBe('2099-02-13')
  })

  test('Can generate correct C4 election dates for the April Special', async () => {
    const february_date = '2099-02-13';
    const april_date = '2099-04-23';
    const primary_date = '2099-08-06';
    const general_date = '2099-11-05';
    const { elections } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date);
    const cAprilSpecialElection = elections.find((p) => p.report_type = 'C4' && p.title.includes('April Special') )
    expect(cAprilSpecialElection).toBeDefined()
    expect(cAprilSpecialElection.election_code).toBe('2099S4')
    expect(cAprilSpecialElection.title).toBe('2099 April Special')
    expect(cAprilSpecialElection.election_date).toBe('2099-04-23')
  })

  test('Can generate correct C4 election dates for the Primary Election', async () => {
    const february_date = '2099-02-13';
    const april_date = '2099-04-23';
    const primary_date = '2099-08-06';
    const general_date = '2099-11-05';
    const { elections } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date);
    const  c4PrimaryElection = elections.find((p) => p.report_type = 'C4' && p.title.includes('Primary') )
    expect(c4PrimaryElection).toBeDefined()
    expect(c4PrimaryElection.election_code).toBe('2099P')
    expect(c4PrimaryElection.title).toBe('2099 Primary')
    expect(c4PrimaryElection.election_date).toBe('2099-08-06')
    expect(c4PrimaryElection.is_primary).toBe(true)
    expect(c4PrimaryElection.is_special).toBe(false)
  })

  test('Can generate correct C4 election dates', async () => {
    const february_date = '2099-02-13';
    const april_date = '2099-04-23';
    const primary_date = '2099-08-06';
    const general_date = '2099-11-05';
    const { elections } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date);
    const  c4GeneralElection = elections.find((p) => p.report_type = 'C4' && p.title.includes('General') )
    expect(c4GeneralElection).toBeDefined()
    expect(c4GeneralElection.election_code).toBe('2099G')
    expect(c4GeneralElection.title).toBe('2099 General')
    expect(c4GeneralElection.election_date).toBe('2099-11-05')
    expect(c4GeneralElection.is_primary).toBe(false)
    expect(c4GeneralElection.is_special).toBe(false)
  })

  test('Can save C4 election dates correctly', async () =>{
    const february_date = '2099-02-13';
    const april_date = '2099-04-23';
    const primary_date = '2099-08-06';
    const general_date = '2099-11-05';
    const { elections } = await generateC4ReportPeriods(february_date, april_date, primary_date, general_date);
    await rp.saveElectionDates(elections)
    const testData = await rds.query(`select title from election where to_char(election_date::date, 'yyyy') = $1`, [2099])
    expect(testData.length).toBe(4)
  })
})