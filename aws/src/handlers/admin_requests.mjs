import {claims, getRestApi, waitForCompletion} from "@wapdc/common/rest-api"
import {CollectionProcessor} from "../CollectionProcessor.mjs";
import {ReportPeriodsProcessor} from "../ReportPeriodsProcessor.mjs";
import path from "path";
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import {getTemplates,setTestEmailAddress} from "@wapdc/common/mailer";
import {generateC4ReportPeriods, generateLobbyistReportPeriodDates} from "../periodsGenerator.mjs"

/**
 * Inside getRestApi sets the CORSHeaders, gets the claims and sets it to the claims variable
 * claims will reflect the requested user data
 */
const api = getRestApi()
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const collectionDir = path.join(__dirname, '..', '..', 'collections');
const cp = new CollectionProcessor(collectionDir)
const rp = new ReportPeriodsProcessor()

/**
 * Make sure we need to have admin rights to get to any of these back-end pages.
 */
api.use('*', (req, res, next) => {
  if (!claims.admin) {
    return res.error({ type: "Access denied", message: "Access denied (admin)", statusCode: 403 })
  }
  setTestEmailAddress(claims.email ?? null)
  next()
})


api.get('/test',async (req,res) => {
 res.json({
   message:"Hello" , claims: claims
 });
})
api.get('/collections/:target', async (req,res) => {
  const {target} = req.params;
  res.json(
      await cp.listCollections(target)
  );
})

/**
 * Create a collection based on data passed.
 * Expects json payload in similiar format:
 *
 * {
 *   template: "name_of_template_to_create"
 *   collection: {
 *     title: "My Collection"
 *     metadata: {
 *       year: "2024"
 *     }
 *   }
 * }
 */
api.post('/create-collection', async (req, res) => {
  const payload = req.body
  const collectionId = await cp.createCollection(payload.template, payload.collection)
  await cp.updateCollectionMembers()
  res.json({
    collectionId
  })
})

api.get('/email-templates', async(req, res) => {
   res.json(await getTemplates())
})

/**
 * Sends email to all members of a collection or specified collection ids.
 * Expects a json payload in the following format:
 *
 * {
 *   template: [
 *     template: 'd-idofemailtemplate',
 *     flag: null
*    ]
 *   bypass: false
 *   flag: both
 *   ids: []
 * }
 *
 * Providing flag limits the sends to those with a specific set of flags.
 */
api.post('/collection/:collection_id/email', async(req, res) => {
  const {collection_id} = req.params;
  const payload = req.body;
  const collection = await cp.getCollection(collection_id);
  for (let i in payload.templates) {
    const {flag, template} = payload.templates[i]
    await cp.sendEmails(collection_id, template, payload.ids ?? null, flag ?? null, payload.bypass ?? false)
  }
  collection.members = await cp.getMembers()
  res.json(collection)
})

api.get('/collection/:collection_id', async(req, res) => {
  const {collection_id} = req.params
  const collection = await cp.getCollection(collection_id)
  collection.members = await cp.getMembers()
  res.json(collection)
})

api.get('/jurisdiction-category', async(req, res) =>{
  const data = await cp.getJurisdictionCategories()
  res.json( data )
})

api.get('/jurisdictions', async(req, res) =>{
  const data = await cp.getJurisdictions()
  res.json( data )
})

api.get('/offices', async(req, res) =>{
  let data = await cp.getOffices()
  res.json( data )
})

api.delete('/collection/:collection_id', async (req, res) => {
  const {collection_id} = req.params
  await cp.removeCollection(collection_id)
  res.json({success: true})
})

api.post('/collection/:collection_id/refresh', async(req,res) =>{
  const {collection_id} = req.params
  const { reset } = req.body
  await cp.getCollection(collection_id)
  if(reset)
    await cp.updateCollectionMembers(true)
  else
    await cp.updateCollectionMembers()
  // Fetch the collection again after the update
  const collection = await cp.getCollection(collection_id)
  collection.members = await cp.getMembers()
  res.json(collection)
})

api.post('/collection/:collection_id/edit-member-info', async(req,res) =>{
    const memberData = req.body
    await cp.updateMemberInfo(memberData)
    res.json({success:true})
  })

api.post('/collection/:collection_id/copy-collection', async (req, res) => {
    const collection_id = req.params.collection_id;
    const collectionTitle = req.body.title;
    const newCollectionId = await cp.copyCollection(collection_id, collectionTitle)
    res.json({ collection_id: newCollectionId })
})

api.post('/collection/:collection_id/update-metadata', async(req, res) => {
  const {collection_id} = req.params
  const {metadata} = req.body
  const collection = {collection_id, metadata}
  await cp.updateDefinition(collection)
  res.json( {success: true})
})

api.get('/case-lookup', async(req,res) => {
  res.json(await cp.caseLookup())
})

api.post('/collection/:collection_id/bulk-edit-member-info', async (req, res) => {
  const collection_id = req.params.collection_id;
  const data = req.body;
  await cp.excludeMembers(collection_id,data)
  res.json({success: true})
})

api.get('/contacts/:target/:target_id', async(req, res) => {
  const { target, target_id } = req.params
  res.json(await cp.getAllContacts(target, target_id))
})

api.post('/collection/update-member-contact/', async(req, res) => {
  const member = req.body
  res.json(await cp.updateMemberInfo(member))
})

api.get('/collection/reporting-period-type/:election_code', async(req, res) =>{
  const { election_code } = req.params
  res.json(await cp.getReportingPeriodType(election_code))
})

api.get('/reports/C4-report-compliance', async(req, res) => {
  const {filing_day = null} = req.query
  res.json(await cp.getC4ReportCount(filing_day))
})

export const lambdaHandler = async( event, context) => {
  const result = await api.run(event,context)
  await waitForCompletion()
  return result
}

api.get('/report/periods/reporting-period-type/:election_code', async(req, res) =>{
    const { election_code } = req.params
    res.json(await rp.getReportingPeriodType(election_code))
})

/**
 * Get all reporting periods for a specific year.
 */
api.get('/report/periods/:reporting_period_year/all', async(req, res) => {
    const {reporting_period_year} = req.params
    res.json(await rp.getReportingPeriods(reporting_period_year))
})

/**
 * Update a reporting period with the given id.
 * Returns a list of reporting periods by year.
 */
api.put('/report/periods/:reporting_period_id/update', async(req, res) => {
    const {reporting_period_id } = req.params;
    const payload = req.body;
    await rp.updateReportingPeriod(reporting_period_id, payload.period)
    res.json(await rp.getReportingPeriods(payload.period.end_date.split("-")[0]))
})

/**
 * Create a new reporting period for the given year.
 */
api.get('/report/periods/generator/:start_year/:end_year/:report_type', async(req, res) => {
    const {start_year, end_year, report_type} = req.params
    res.json(await generateLobbyistReportPeriodDates(start_year, end_year, report_type))
})

/**
 * Generate C4 report periods.
 * I define a helper function takes a date string & returns years.
 * I extract the year from the payload use it to heck the April
 * special, primary and general years & if 1 or more is different
 * throw an error.
 */
api.post('/report/periods/generator/c4', async(req, res) => {
    const payload = req.body
    const getYear = dateStr => new Date(dateStr).getFullYear()
    const payloadYear = getYear(payload.february_date)

    if (getYear(payload.april_date) !== payloadYear ||
        getYear(payload.primary_date) !== payloadYear ||
        getYear(payload.general_date) !== payloadYear) {
        throw new Error('All dates in the must be within the same year');
    }

    res.json(await generateC4ReportPeriods(payload.february_date, payload.april_date, payload.primary_date, payload.general_date))
})

/**
 * Get all reporting periods for all years.
 */
api.get('/report/periods/all/years', async(req, res) => {
    res.json(await rp.getAllReportingPeriods())
})

/**
 * Saves all the rows that are generated from the report period generator
 */
api.put('/report/periods/save/dates', async(req, res) => {
    const payload = req.body
    res.json(await rp.saveReportingPeriods(payload))
})

api.put('/report/periods/save/election/dates', async(req, res) => {
    const payload = req.body
    res.json(await rp.saveElectionDates(payload))
})