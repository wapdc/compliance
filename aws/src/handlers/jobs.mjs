import {CollectionProcessor} from "../CollectionProcessor.mjs";
import * as rds from '@wapdc/common/wapdc-db'
import path, {dirname} from "path";
import {fileURLToPath} from "url";

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const collectionDir = path.join(__dirname, '..', '..', 'collections');
const cp = new CollectionProcessor(collectionDir)
export async function  dailyTasks() {
  await rds.connect()
  let result = { statusCode: 500 }
  const primaryReportPeriodTypes = [
    'PostPrimary',
    'SevenDayPrePrimary',
    'TwentyOneDayPrePrimary'
  ]
  const generalReportPeriodTypes = [
    'PostGeneral',
    'SevenDayPreGeneral',
    'TwentyOneDayPreGeneral'
  ]

  try {
    const stage = process.env.STAGE
    await cp.sendReportReminders('L2', ['Monthly'],'lobbyist-firms-monthly-reports-due-for-period', cp.collectionFromL2ReportPeriod, stage === 'prod')
    await cp.sendReportReminders('C4', primaryReportPeriodTypes,'candidacy-missing-C4-reports-primary', cp.collectionFromC4ReportPeriodPrimary, false)
    await cp.sendReportReminders('C4', generalReportPeriodTypes,'candidacy-missing-C4-reports', cp.collectionFromC4ReportPeriodGeneral, false)
    result = {statusCode: 200}
  }
  catch (e) {
    result.error = e
  }
  finally {
    await rds.close()
  }
  return result
}