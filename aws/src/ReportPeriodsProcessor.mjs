import * as rds from '@wapdc/common/wapdc-db'
/**
 * Handles the generation and manipulation of reporting
 * periods and election dates.
 */
class ReportPeriodsProcessor {
  async getReportingPeriodType(election_code) {
    return rds.query(`select r.reporting_period_id as id, r.reporting_period_type as type
                      from election e
                             join reporting_period r on r.election_date = e.election_date
                      where r.report_type = 'C4'
                        and e.election_code = $1
                      Order by start_date`, [election_code]
    );
  }

  /**
   * Helper function for reporting periods generation returns the max year for a L2 and L3
   * We don't need to worry about C4 because it's based on election dates
   * @returns {Promise<*>}
   */
  async getAllReportingPeriods() {
    return rds.query(`select max(report_type) as report_type, max(extract(year from due_date)) + 1 as year
                      from reporting_period
                      where report_type = 'L3'
                      union all
                      select max(report_type) as report_type, max(extract(year from due_date)) + 1 as year
                      from reporting_period
                      where report_type = 'L2'`);
  }

  /**
   * Get a list of all reporting periods for a given year
   * @param reporting_period_year
   * @returns {Promise<*>}
   */
  async getReportingPeriods(reporting_period_year) {
    return rds.query(`(
    select *
      from reporting_period
      where extract(year from end_date) = $1
        and report_type = 'L2' order by due_date)
      UNION ALL
      (select *
      from reporting_period
      where extract (year from end_date) = $1
        and report_type = 'L3' order by due_date)
      UNION ALL
      (select *
      from reporting_period
      where extract (year from election_date) = $1
        and report_type = 'C4' order by election_date)
      UNION ALL
      (select *
      from reporting_period
      where extract (year from end_date) = $1
        and report_type = 'C4' and reporting_period.reporting_period_type = 'Monthly' order by due_date)`,
              [reporting_period_year]);
  }

  /**
   * Update a reporting period in the database
   * The start date's year is used to ensure all dates submitted are within the same year. In some cases that date may have
   *   a year that falls within the previous year.
   * We account for the election_date because it is not always present (such as in a L2, L3)
   * The reminder_date is not always present on a C4, L2 or L3
   * The due_dates do not always fall in the same year, such as a December Monthly C4
   * The election_dates do not always fall in the same year, such as in January
   * @param reporting_period_id
   * @param period_data
   * @returns {Promise<void>}
   */
  async updateReportingPeriod(reporting_period_id, period_data) {
    if (!period_data.reporting_period_id) {
      throw new Error("Period id missing")
    }

    const getYear = dateStr => (dateStr).split('-')[0]

    const payloadYear = getYear(period_data.start_date)
    try {
      this.isDateInPayloadYear(period_data.election_date, payloadYear, period_data.reporting_period_type, ['TwentyOneDayPreElection']);
      this.isDateInPayloadYear(period_data.reminder_date, payloadYear, period_data.reporting_period_type, ['TwentyOneDayPreElection', 'Monthly', 'Annual']);
      this.isDateInPayloadYear(period_data.end_date, payloadYear, period_data.reporting_period_type, ['TwentyOneDayPreElection', 'Monthly', 'Annual']);
      this.isDateInPayloadYear(period_data.due_date, payloadYear, period_data.reporting_period_type, ['Annual', 'TwentyOneDayPreElection', 'Monthly']);
    } catch (error) {
      console.error(error.message);
    }

    const parameters = [period_data.reporting_period_id, period_data.election_date, period_data.start_date, period_data.end_date, period_data.due_date, period_data.reminder_date]
    return rds.execute(`UPDATE reporting_period
                        SET election_date = $2,
                            start_date    = $3,
                            end_date      = $4,
                            due_date      = $5,
                            reminder_date = $6
                        WHERE reporting_period_id = $1`, parameters)
  }

  /**
   * Checks if the reporting period type is not one of the excluded types
   *   and if the date is not within the payload year throw an error.
   * @param date
   * @param payloadYear
   * @param reportingPeriodType
   * @param excludedTypes
   * @returns {Promise<void>}
   */
  async isDateInPayloadYear(date, payloadYear, reportingPeriodType, excludedTypes = []) {
    // Check if the reporting period type is not one of the excluded types
    // and if the date is not within the payload year, throw an error.
    const getYear = dateStr => (dateStr).split('-')[0]
    if (date !== null && !excludedTypes.includes(reportingPeriodType) && getYear(date) !== payloadYear) {
      throw new Error(`${date} must be within the same year of ${payloadYear}`);
    }
  }

  /**
   * Saves reporting period data to the database.
   *   code expects rows of data tobe either a single row an array.
   * The reminder & election dates aren't required and should have a default
   *   value of null if left empty.
   * @param reporting_periods
   * @returns {Promise<void>}
   */
  async saveReportingPeriods(reporting_periods) {
    const formatValue = (value) => {
      if (value === null || value === undefined) {
        return 'NULL';
      }
      return `'${value}'`;
    };

    const formatPeriod = (period) => {
      return `(
        '${period.reporting_period_type}',
        ${formatValue(period.election_date)},
        '${period.start_date}',
        '${period.end_date}',
        '${period.due_date}',
        '${period.report_type}',
        ${formatValue(period.reminder_date)})`;
    };

    const generateInsertQuery = (reporting_periods) => {
      const values = Array.isArray(reporting_periods)
          ? reporting_periods.map(formatPeriod).join(', ')
          : formatPeriod(reporting_periods);

      return `
        INSERT INTO reporting_period (reporting_period_type, election_date, start_date, end_date,
                                      due_date, report_type, reminder_date) VALUES ${values}`
    };

    const query = generateInsertQuery(reporting_periods);
    await rds.execute(query);
  }

  async saveElectionDates(elections) {

    if(!Array.isArray(elections)){
      throw new Error('Missing a date')
    }
   const values = elections.map(election => {
     if(election.election_code.substring(0,4) !== election.election_date.split('-')[0] ){
       throw new Error('Election code and election dates must be in the same year')
     }
     return `(
            '${election.election_code}', 
            '${election.title}',
            '${election.is_primary}',
            '${election.is_special}',
            '${election.election_date}'
         )`
      }).join(', ');

    await rds.execute(
        `INSERT INTO election ( election_code, title, is_primary, is_special, election_date) VALUES ${values}`);
  }
}

export { ReportPeriodsProcessor }
