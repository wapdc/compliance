import * as rds from '@wapdc/common/wapdc-db'
import yaml from 'js-yaml'
import fs from 'fs'
import {addRecipient, sendMailMerge, setMessageTemplate} from "@wapdc/common/mailer"
import moment from "moment/moment.js"

/**
 * Transform a tokenized parameter query into
 * @param parameterizedSql
 * @param params
 * @returns {{values: (*|*[]|number), text: (*|*[]|number)}}
 */
function queryConvert(parameterizedSql, params) {
  const [text, values] = Object.entries(params).reduce(
    ([sql, array, index], [key, value]) => [sql.replaceAll(`:${key}`, `$${index}`), [...array, value], index + 1],
    [parameterizedSql, [], 1]
  );
  return { text, values };
}

function buildSetClause(object, columns, parameters=[]) {
  const sets = []
  let index = parameters.length;
  columns.forEach(col => {
    if (object.hasOwnProperty(col)) {
      index++;
      sets.push(`${col}=\$${index}`)
      if (col === 'metadata') {
        parameters.push(JSON.stringify(object.metadata))
      }
      else {
        parameters.push(object[col])
      }
    }
  })

  return sets.join(', ');
}


/**
 * Handles manufacturing and manipulation of collections.
 */
class CollectionProcessor {
  constructor(dir) {
    this.dir = dir
    this.collection_id = null
    this.metadata = {}
  }

  /**
   * Create a collection from a template file name and collection metadata
   *
   * @param templateFile
   *   Base name of yaml file to be used in collections
   * @param data
   *   Data to be used during collection creation
   * @returns {Promise<*>}
   */
  async createCollection(templateFile, data = {}) {
    // Load collection definition
    const fileName = templateFile + '.yml'
    const fileContents = fs.readFileSync(this.dir + '/' + fileName)
    let definition = this.definition = yaml.load(fileContents)

    // Build the row
    const group = data?.group ?? definition.group
    const key = data?.key ?? definition.key
    const target = definition.target
    const category = data?.category ?? definition.category
    this.metadata = Object.assign( definition.metadata ?? {}, data.metadata)
    const metaData = JSON.stringify(this.metadata)
    const title = data?.title ?? definition.title
    // Check if collection defaults file exists
    const collectionDefaultsFileName = target + "-defaults.yml";
    if (fs.existsSync(this.dir + '/' + collectionDefaultsFileName)) {
      const collectionDefaultsContents = fs.readFileSync(this.dir + '/' + collectionDefaultsFileName);
      this.definition = {...yaml.load(collectionDefaultsContents), ...this.definition,};
    }
    const collection = await rds.fetch(`
      insert into collection(collection_group, collection_key, title, target, category, definition, metadata)
      VALUES($1, $2, $3, $4, $5, $6, $7) returning collection_id
     `, [ group, key, title, target, category, fileName, metaData])
    this.collection_id = collection.collection_id
    return this.collection_id;
  }

  async listCollections(target)  {
   return rds.query(`select
      c.*,
      cm.members
      from collection c
      left join (select collection_id, count(*)::int as members from collection_member where not excluded group by collection_id ) cm on cm.collection_id=c.collection_id
      where category in ('outreach', 'reporting-modification', 'custom')
        and (cast ($1 as text) is null or target=$1) 
      order by collection_group desc, updated_at desc`, [target]);
  }

  async processMembers(source) {
     if (!source.source) {
       throw 'No sql file specified'
     }
     if (source.mode !== 'execute' && !source.id_field) {
       throw 'Missing Id Field'
     }
    const fileContents = fs.readFileSync(this.dir + '/' + source.source).toString()

    // Build the fields that need to be in SQL
    const id_field           = source.id_field ?? "CAST(NULL as int)";
    const label_field        = source.label_field ?? id_field;
    const email_field        = source.email_field ?? "CAST(NULL as text)";
    const address_field      = source.address_field ?? "CAST(NULL as text)";
    const city_field         = source.city_field ?? "CAST(NULL AS text)";
    const state_field        = source.state_field ?? "CAST(NULL as text)";
    const postcode_field     = source.postcode_field ?? "CAST(NULL as text)";
    const phone_field        = source.phone_field ?? "CAST(NULL as text)";
    const compare_field      = source.compare_id_field ?? "CAST(NULL as int)";
    const score_field        = source.score_field ?? "CAST(NULL as int)";
    const flag_field         = source.flag_field ?? "CAST(NULL as text)";
    const excluded_field     = source.excluded_field ?? "CAST(NULL as boolean)";
    const removal_field      = source.removal_field ?? "CAST (NULL as boolean)";
    const office_field       = source.office_field ?? "CAST (NULL AS varchar)";
    const jurisdiction_field = source.jurisdiction_field ?? "CAST(NULL as int)";
    const metadata_field     = source.metadata_field ?? "CAST(NULL as text)";
    let parameters = {};
    // transfer parameters
    if (source.parameters) {
      source.parameters.forEach(parameter => {
        parameters[parameter] = this.metadata.hasOwnProperty(parameter) ? this.metadata[parameter] : null;
      })
    }

    // Trim off trailing ;
    const sql = fileContents.replace(/;+$/, "");

    let memberQuery
    switch (source.mode) {
      case "add":
        parameters.collection_id = this.collection_id
        memberQuery = `
          INSERT INTO collection_member(collection_id, target_id, compare_id, 
                                        label, email, address, city, state, postcode, phone, 
                                        flag, score, updated_at, excluded, jurisdiction_id, office_code, metadata) 
          select CAST(:collection_id AS INTEGER), ${id_field}, ${compare_field}, ${label_field}, ${email_field}, ${address_field},
                 ${city_field}, ${state_field}, ${postcode_field}, ${phone_field}, ${flag_field}, ${score_field}, now(), false, ${jurisdiction_field},
                 ${office_field}, ${metadata_field}
          FROM (${sql}) c ON CONFLICT (collection_id, target_id) DO UPDATE set email=COALESCE(collection_member.email, excluded.email)
        `;
        break;
      case "update":
        parameters.collection_id = this.collection_id
        memberQuery = `update collection_member t set excluded = COALESCE(v.update_excluded,excluded),
        flag=COALESCE(CAST(v.update_flag as varchar), flag),
          compare_id = COALESCE(CAST(v.update_compare_id as int), compare_id),
          score = COALESCE(CAST(v.update_score as int), score),
          email = COALESCE(v.update_email, t.email),
          phone = COALESCE(v.update_phone, t.phone),
          address = COALESCE(v.update_address, t.address),
          city = COALESCE(v.update_city, t.city),
          state = COALESCE(v.update_state, t.state),
          postcode = COALESCE(v.update_postcode, t.postcode),
          metadata = COALESCE(v.update_metadata, t.metadata),
          updated_at = now()
        FROM
        (select CAST(:collection_id AS INTEGER) AS collection_id,
        ${id_field} as target_id,
        ${flag_field} as update_flag,
        ${excluded_field} as update_excluded,
        ${compare_field} as update_compare_id,
        ${score_field} as update_score,
        ${email_field} as update_email,
        ${phone_field} as update_phone,
        ${address_field} as update_address,
        ${city_field} as update_city,
        ${state_field} as update_state,
        ${postcode_field} as update_postcode,
        ${metadata_field} as update_metadata
        FROM (${sql}) c ) v
        WHERE v.collection_id = t.collection_id and v.target_id = t.target_id`;
        break;
      case "remove":
        parameters.collection_id = this.collection_id;
        memberQuery = `
        delete from collection_member where member_id in (
            select member_id from (
                select CAST(:collection_id AS INTEGER) AS collection_id, 
                       member_id, 
                       ${removal_field} as removal_flag
                FROM (${sql}) c ) v
            where v.removal_flag = true and collection_id = v.collection_id and collection_member.member_id = v.member_id)
            `;
        break;
      case "execute":
        memberQuery = sql;
        break;
      default:
        throw "Invalid mode: " + source.mode
    }
    const query = queryConvert(memberQuery, parameters)
    await rds.execute(query.text, query.values)
  }


  async performAction(action, reset = false) {
     const collectionId = this.collection_id;
     const definition = this.definition;
     if (!this.definition) {
       throw 'Missing collection definition'
     }
     if (!definition.actions || !definition.actions[action]) {
       throw 'Invalid collection action: ' + action
     }
     if (reset) {
       await rds.execute(`DELETE FROM collection_member WHERE collection_id = $1`, [collectionId])
     }
     await rds.execute(`UPDATE collection SET updated_at = now() where collection_id = $1`, [collectionId])
     for (const source of definition.actions[action]) {
       await this.processMembers(source)
     }
  }

  async copyCollection(originalCollectionId, newCollectionTitle) {
    const collection = await rds.fetch("select * from wapdc.public.collection where collection_id = $1", [originalCollectionId])
    if (!collection) { throw new Error('Original collection not found'); }
    collection.collection_id = null;
    collection.title = newCollectionTitle;
    collection.collection_key = null;
    collection.updated_at = Date.now()

    // Build the row
    const group = collection.collection_group
    const key = collection.collection_key
    const target = collection.target
    const category = collection.category
    const fileName = collection.definition
    const title = collection?.title
    const { collection_id } = await rds.fetch(`
      INSERT INTO collection(collection_group, collection_key, title, target, category, definition, metadata)
      VALUES ($1, $2, $3, $4, $5, $6, $7)
      RETURNING collection_id`, [group, key, title, target, category, fileName, collection.metadata])

      await rds.execute(`INSERT INTO collection_member (collection_id, target_id, compare_id, label, email, address, city,
                                                state, postcode, phone, flag, score, updated_at, excluded, memo,
                                                jurisdiction_id, office_code, metadata) 
        SELECT $1, target_id, compare_id, label, email, address, city,
                                                state, postcode, phone, flag, score, updated_at, excluded, memo,
                                                jurisdiction_id, office_code, metadata from collection_member
                                                where collection_id = $2`,[collection_id, originalCollectionId])

    return collection_id;
  }

  updateCollectionMembers(reset = false) {
    return this.performAction('populate', reset)
  }


  /**
   * Retrieve a collection from the database
   * @param collectionId
   * @returns {Promise<Object>}
   */
  async getCollection(collectionId){
    const collection = await   rds.fetch(`
      SELECT c.collection_id, c.definition as template, c.category, c.collection_group as "group", 
             c.collection_key as key, c.target, c.title, c.updated_at, CAST(c.metadata as JSON) as metadata 
      FROM collection c WHERE collection_id=$1
    `, [collectionId])
    this.collection_id = collectionId
    if (!collection) {
      throw "Collection not found!"
    }
    this.metadata = collection.metadata ?? {}
    const fileName = collection.template
    const fileContents = fs.readFileSync(this.dir + '/' + fileName)
    collection.definition = this.definition = yaml.load(fileContents)
    // Check if collection defaults file exists
    const collectionDefaultsFileName = collection.target + "-defaults.yml";
    if (fs.existsSync(this.dir + '/' + collectionDefaultsFileName)) {
      const collectionDefaultsContents = fs.readFileSync(this.dir + '/' + collectionDefaultsFileName);
      collection.definition = this.definition = { ...yaml.load(collectionDefaultsContents), ...collection.definition };
    }
    return collection
  }

  async getJurisdictionCategories(){
    return rds.query(`SELECT jurisdiction_category_id, short_name FROM jurisdiction_category order by short_name`);
  }

  async getJurisdictions(){
    return rds.query(`select j.jurisdiction_id, j.name , j.county, c.coname, j.rpts, jc.short_name as category, jc.jurisdiction_category_id,
                               case when j.inactive is not null then 'N' when valid_to < now() then 'N' else 'Y' end as active
                                from jurisdiction j
                                       left join fcounty c
                                                 on j.county = c.conum
                                       left join jurisdiction_category jc
                                                 on jc.jurisdiction_category_id = j.category
                                order by j.name`);
  }

  async getOffices(){
    return rds.query(`SELECT offcode, offtitle FROM foffice order by offtitle`);
  }

  async removeCollection(collectionId){
    return rds.execute(`delete from collection where collection_id=$1`, [collectionId])
  }

  async updateDefinition(collection) {
    const title = collection.title ?? null
    const metadata = collection.metadata ? JSON.stringify(collection.metadata) : null
    const template = collection.template ?? null
    if (!collection.collection_id) {
      throw "Collection id missing"
    }
    if (template && !fs.existsSync(this.dir + '/' + template)) {
      throw "Unknown collection definition: " + template
    }
    return rds.execute(`UPDATE collection 
         SET title=COALESCE($2, title), metadata=COALESCE($3, metadata),
             definition=COALESCE($4, definition)
         WHERE collection_id = $1`, [collection.collection_id, title, metadata, template])
  }

  async getUniqueCollection(target, category, key) {
    const {collection_id} = await rds.fetch(`SELECT collection_id from collection where target=$1 and category=$2 and collection_key=$3`,
      [target, category, key])
    return this.getCollection(collection_id)
  }


  async updateMemberInfo(member) {
    if (!member.collection_id) {
      throw "No collection id"
    }
    if (!member.target_id) {
      throw "No target id"
    }
    const parameters = [member.target_id, member.collection_id]
    const setClause = buildSetClause(member, ['flag', 'excluded',  'memo', 'office_code', 'jurisdiction_id', 'address', 'city', 'state', 'postcode','metadata', 'compare_id', 'email'], parameters)
    if (setClause) {

      return rds.execute(`UPDATE collection_member set ${setClause} WHERE target_id=$1 and collection_id=$2`, parameters)
    }
  }

  async sendEmails(collectionId, template, ids = null, flag, bypass=false) {
    if (!collectionId || !template) {
      throw Error("Missing Required fields for email send")
    }
    let query
    let parameters
    const {metadata} = await rds.fetch(`select cast(metadata as json) as metadata from collection where collection_id=$1`, [collectionId])
    if (ids) {
      query = `select * from collection_member where collection_id = $1 and coalesce($2, flag) is not distinct from flag and member_id = any($3::int[]) and email is not null `
      parameters = [collectionId, flag, ids]
    }
    else {
      query = `select * from collection_member where collection_id = $1 and excluded=false and coalesce($2, flag) is not distinct from flag and email is not null`
      parameters = [collectionId, flag]
    }

    const rows = await rds.query(query, parameters)

    if (rows && rows.length) {
      await setMessageTemplate(template, bypass)
      rows.forEach(row => {
        const data =  {
          name: row.label
        }
        if (metadata) {
          Object.assign(data,metadata)
        }
        if (row.metadata) {
          Object.assign(data,JSON.parse(row.metadata))
        }
        if (row.email) {
          const emails = row.email.replace(' ', '').split(';')
          addRecipient(emails, data)
        }
      })
      await sendMailMerge()
    }

    // Mark the members as having been contacted.
    await rds.execute(`update collection_member set last_contacted=now() where collection_id=$1 and member_id in (select member_id from (${query}) m)`, parameters)
  }

  async getMembers() {
    const memberQuery = this.definition.memberQuery ?? 'collection-members.sql'
    const fileContents = fs.readFileSync(this.dir + '/' + memberQuery).toString()
    // Trim off trailing ;
    const sql = fileContents.replace(/;+$/, "");

    const query = queryConvert(sql, {collection_id: this.collection_id})
    return rds.query(query.text, query.values)
  }

  async caseLookup() {
    const {area_of_law, political_category, group_enforcement} = await rds.fetch(`
        select
            (select json_agg(v1 order by field_value) from compliance_case_fields_lookup v1 where field_type='area_of_law') as area_of_law,
            (select json_agg(v2 order by field_value) from compliance_case_fields_lookup v2 where field_type='political_category') as political_category,
            (select json_agg(v3 order by field_value) from compliance_case_fields_lookup v3 where field_type='group_enforcement') as group_enforcement
    `)
    political_category.forEach(category => {
      const value = category.field_value;

      // Set type field.
      if (value.includes('Candidate')) {
        category.type = 'candidate'
      }
      else if (value.includes('Political Committee')) {
        category.type = 'committee'
      }
      else if (value.includes('Public Employee')) {
        category.type = 'official'
      }
      else if (value.includes('PUblic Agency')) {
        category.type = 'agency'
      }
      else {
        category.type = 'other'
      }

      // set jurisdiction categories
      if (value.includes('State')) {
        category.jurisdiction_categories = [1, 14, 15]
      }
      else if (value.includes('Judicial')) {
        category.jurisdiction_categories = [5]
      }
      else if (value.includes('Legislative')) {
        category.jurisdiction_categories = [4]
      }
      else if (value.includes('Local')) {
        category.jurisdiction_categories = [2, 3, 6, 7, 8, 9, 10, 11, 12, 13, 16]
      }
      else {
        category.jurisdiction_categories = []
      }

    })
    return {area_of_law, political_category, group_enforcement}
  }
  async excludeMembers(collectionId,payload)  {
    if (!collectionId) {
      throw "No collection id"
    }
    if (payload.ids.length === 0) {
      throw "No target id"
    }
    const memo = payload.bulkMemoText ? payload.bulkMemoText : null;
    const parameters = [collectionId,payload.excluded,memo,payload.ids]
    return rds.execute(`update collection_member set memo = coalesce($3,memo), excluded=$2
                        where collection_id=$1 and target_id = ANY ($4)`, parameters)
  }

  async getEntityIdByType(target, target_id) {
    let query;
    switch (target) {
      case 'lobbyist_firm':
        query = "SELECT entity_id FROM lobbyist_firm WHERE lobbyist_firm_id=$1"
        break;
      case 'candidacy':
        query = "SELECT person_id AS entity_id FROM candidacy WHERE candidacy_id=$1"
        break;
      case 'person':
        query = "SELECT entity_id FROM entity WHERE entity_id=$1"
        break;
      case 'lobbyist_client':
        query = "SELECT entity_id FROM lobbyist_client WHERE lobbyist_client_id=$1"
        break;
      case 'officials':
        query = "SELECT person_id AS entity_id FROM official WHERE official_id=$1"
        break;
      case 'committee':
        query = "SELECT person_id AS entity_id FROM committee WHERE committee_id=$1"
        break;
      default:
        break;
    }

    const { entity_id } = await rds.fetch(query, [target_id])
    return entity_id
  }

  async getAllContacts(target, target_id) {
    const entity_id = await this.getEntityIdByType(target, target_id);
    const allContactsQuery = 'entity-get-all-contacts.sql';
    const fileContents = fs.readFileSync(`${this.dir}/${allContactsQuery}`).toString();
    const sql = fileContents.replace(/;+$/, "");
    const query = queryConvert(sql, { entity_id: entity_id });

    return await rds.query(query.text, query.values);
  }

  async sendReportReminders(report_type,report_period_type, definition, collectionDataFunction, sendMails = true) {
    let collection = {}
    // Find the reporting periods that we haven't sent a reminder for.
      const periods = await rds.query(
        `select * from reporting_period where report_type = $1 and reporting_period_type = any($2)  and reminder_date <= now() and reminder_sent_date is null`,
        [report_type, report_period_type]
      )

    if (periods) {
      for (const period of periods ) {
        collection = await collectionDataFunction.call(this, period)
        if(!collection.exists){
          const collectionData = collection.data
          console.info("Creating collection", collectionData)
          await this.createCollection(definition, collectionData)
          await this.updateCollectionMembers()
          if (sendMails) {
            await this.sendEmails(this.collection_id,this.definition?.emailTemplate?.default, null, null, false)
          }
        }
        await rds.execute('update reporting_period set reminder_sent_date=now() where reporting_period_id=$1 ', [period.reporting_period_id])
      }
    }
  }

  collectionFromL2ReportPeriod(period) {
    if (!period) {
      throw 'No reporting period'
    }

    const start_date = new Date(moment(period.start_date))

    const key = `${period.report_type} ${start_date}`
    const month = start_date.getMonth() + 1
    const year = start_date.getFullYear()
    const monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];

    const title = `${monthNames[month - 1]} ${year} ${period.report_type} reminder`
    return {
      exists: false,
      data: {
        title,
        key,
        group: year.toString(),
        metadata: {
          year,
          month,
          period_name: `${monthNames[month - 1]} ${year}`,
        }
      }
    }
  }

  async checkCollectionExists(definition, target, category, key) {
    return await rds.fetch(`select * from collection where definition = $1 and target = $2 and category = $3 and collection_key = $4`,
      [definition, target, category, key])
  }

  collectionFromC4ReportPeriodPrimary(period) {
    return this.collectionFromC4ReportPeriod(period, true)
  }

  collectionFromC4ReportPeriodGeneral(period) {
    return this.collectionFromC4ReportPeriod(period, false)
  }
   formatC4ReportType(type) {
     const mappings = {
       "TwentyOneDayPreGeneral": "21-day pre-general",
       "PostGeneral": "post-general",
       "TwentyOneDayPreElection": "21-day pre-election",
       "SevenDayPreGeneral": "7-day pre-general",
       "PostPrimary": "post-primary",
       "TwentyOneDayPrePrimary": "21-day pre-primary",
       "SevenDayPrePrimary": "7-day pre-primary",
       "SevenDayPreElection": "7-day pre-election",
       "PostElection": "post-election"
     }
     return mappings[type] || type
  }
  async collectionFromC4ReportPeriod(period, isPrimary) {
    if (!period) {
      throw 'No reporting period'
    }
    const templateName = isPrimary ? `candidacy-missing-C4-reports-primary` : `candidacy-missing-C4-reports`
    const key = `${templateName}-${period.reporting_period_id}`
    const year = new Date(period.start_date).getFullYear()
    const definition = `${templateName}.yml`
    const collectionExists = await this.checkCollectionExists(definition, 'candidacy', 'outreach', key)
    const title = `Missing ${this.formatC4ReportType(period.reporting_period_type)} - ${year}`

    if (collectionExists) {
      return {
        exists: true,
        collection_id: collectionExists.collection_id,
        definition,
        key
      }
    }
    return {
      exists: false,
      data: {
        key,
        title,
        group: year.toString(),
        metadata: {
          election_code: year,
          reporting_period_id: period.reporting_period_id,
          start_date: period.start_date,
          end_date: period.end_date,
          report_name: this.formatC4ReportType(period.reporting_period_type),
          due_date: period.due_date
        }
      }
    }
  }

  async getReportingPeriodType(election_code){
    return rds.query(`select r.reporting_period_id as id, r.reporting_period_type as type, r.start_date, r.end_date,r.due_date from election e 
                      join reporting_period r on r.election_date = e.election_date 
                      where r.report_type = 'C4' and e.election_code = $1 Order by start_date`,[election_code]
    );
  }

  async getC4ReportCount(date) {
    const fileName = 'candidacy-C4-report-count.sql'
    const fileContents = fs.readFileSync(this.dir + '/' + fileName, 'utf8')
    return rds.query(fileContents, [date])
  }
}

export { CollectionProcessor }
