<script setup>
import { reactive, ref } from 'vue';
import { useCompliance } from 'src/compliance';

const props = defineProps(['row', 'col', 'collection_id', 'collection']);
const { awsGateway } = await useCompliance();

const row = reactive(props.row);
const addresses = ref([]);
const selectedAddress = ref(null);
const showDialog = ref(false);
const collection = props.collection;

/**
 * Normalizes an address for comparison by converting it to lowercase, removing non-alphanumeric characters
 * (except for the # symbol), replacing multiple spaces with a single space, and trimming
 * leading and trailing spaces.
 * @param {string} address - The address to normalize.
 * @returns {string} The normalized address for comparison.
 */
const normalizeAddress = (address) => {
  return address.toLowerCase().replace(/[^a-z0-9#]/g, '').replace(/\s+/g, '');
};

/**
 * Normalizes the state for comparison by converting it to its corresponding abbreviation if it's a full state name.
 * If the state is already abbreviated or not found in the list, the original input is returned in lowercase.
 * @param {string} state - The full name or abbreviation of the state.
 * @returns {string} The abbreviated state in lowercase if found, otherwise the original input in lowercase.
 */
const normalizeState = (state) => {
  const stateAbbreviations = {
    'ALABAMA': 'AL', 'ALASKA': 'AK', 'ARIZONA': 'AZ', 'ARKANSAS': 'AR', 'CALIFORNIA': 'CA',
    'COLORADO': 'CO', 'CONNECTICUT': 'CT', 'DELAWARE': 'DE', 'FLORIDA': 'FL', 'GEORGIA': 'GA',
    'HAWAII': 'HI', 'IDAHO': 'ID', 'ILLINOIS': 'IL', 'INDIANA': 'IN', 'IOWA': 'IA',
    'KANSAS': 'KS', 'KENTUCKY': 'KY', 'LOUISIANA': 'LA', 'MAINE': 'ME', 'MARYLAND': 'MD',
    'MASSACHUSETTS': 'MA', 'MICHIGAN': 'MI', 'MINNESOTA': 'MN', 'MISSISSIPPI': 'MS', 'MISSOURI': 'MO',
    'MONTANA': 'MT', 'NEBRASKA': 'NE', 'NEVADA': 'NV', 'NEW HAMPSHIRE': 'NH', 'NEW JERSEY': 'NJ',
    'NEW MEXICO': 'NM', 'NEW YORK': 'NY', 'NORTH CAROLINA': 'NC', 'NORTH DAKOTA': 'ND', 'OHIO': 'OH',
    'OKLAHOMA': 'OK', 'OREGON': 'OR', 'PENNSYLVANIA': 'PA', 'RHODE ISLAND': 'RI', 'SOUTH CAROLINA': 'SC',
    'SOUTH DAKOTA': 'SD', 'TENNESSEE': 'TN', 'TEXAS': 'TX', 'UTAH': 'UT', 'VERMONT': 'VT',
    'VIRGINIA': 'VA', 'WASHINGTON': 'WA', 'WEST VIRGINIA': 'WV', 'WISCONSIN': 'WI', 'WYOMING': 'WY',
  };
  return (stateAbbreviations[state.toUpperCase()] || state).toLowerCase();
};

/**
 * Normalizes the suffixes in an address for comparison by converting common full forms to their abbreviations.
 * If a suffix is not found in the list of mappings, it is left unchanged.
 * @param {string} address - The address with suffixes to normalize.
 * @returns {string} The address with normalized suffixes for comparison.
 */
const normalizeSuffix = (address) => {
  const suffixMappings = {
    'STREET': 'ST', 'AVENUE': 'AVE', 'ROAD': 'RD', 'DRIVE': 'DR', 'LANE': 'LN',
    'BOULEVARD': 'BLVD', 'COURT': 'CT', 'PLACE': 'PL', 'TERRACE': 'TER', 'PARKWAY': 'PKWY'
  };
  const words = address.split(' ');
  const normalizedWords = words.map(word => (suffixMappings[word.toUpperCase()] || word).toLowerCase());
  return normalizedWords.join(' ');
};

/**
 * Normalizes a string by removing all non-alphanumeric characters, converting to lowercase,
 * and trimming excess whitespace.
 * @param {string} str - The string to normalize.
 * @returns {string} The normalized string.
 */
const normalizeString = (str) => {
  return str.replace(/[^a-z0-9]/gi, '').toLowerCase().trim();
};

/**
 * Creates a unique identifier for an address by normalizing its components.
 * @param {Object} addressObj - The address object containing address, city, state, and postcode.
 * @returns {string} The unique identifier for the address.
 */
const createAddressIdentifier = ({ address, city, state, postcode }) => {
  const normalizedAddress = normalizeString(address);
  const normalizedCity = normalizeString(city);
  const normalizedState = normalizeString(state);
  const normalizedPostcode = normalizeString(postcode);
  return `${normalizedAddress}|${normalizedCity}|${normalizedState}|${normalizedPostcode}`;
};

/**
 * Fetches addresses from the API based on the target ID and removes duplicates.
 * @param {string} target_id - The target ID for which to fetch addresses.
 */
const fetchAddresses = async (target_id) => {
  const fetchedData = await awsGateway.get(`/contacts/${props.collection.target}/${target_id}`);
  if (fetchedData) {
    const addressMap = new Map();
    fetchedData
      .filter(({ address, city, state, postcode }) =>
        [address, city, state, postcode].every(field => field != null && field.trim() !== '')
      )
      .forEach(addressObj => {
        const addressIdentifier = createAddressIdentifier(addressObj);
        if (!addressMap.has(addressIdentifier)) {
          addressMap.set(addressIdentifier, addressObj);
        }
      });
    addresses.value = Array.from(addressMap.values());
  }
};

/**
 * Opens the address selection dialog and sets the selected address.
 */
const openDialog = async () => {
  await fetchAddresses(props.row.target_id);
  const currentAddress = {
    address: row.address,
    city: row.city,
    state: row.state,
    postcode: row.postcode,
  };

  // Normalize the current address for comparison
  const normalizedCurrentAddress = `${normalizeSuffix(normalizeAddress(currentAddress.address))}|${normalizeAddress(currentAddress.city)}|${normalizeState(currentAddress.state)}|${normalizeAddress(currentAddress.postcode)}`;

  // Find the address in the fetched addresses that matches the normalized current address
  const matchingAddress = addresses.value.find(addr => {
    const normalizedFetchedAddress = `${normalizeSuffix(normalizeAddress(addr.address))}|${normalizeAddress(addr.city)}|${normalizeState(addr.state)}|${normalizeAddress(addr.postcode)}`;
    return normalizedCurrentAddress === normalizedFetchedAddress;
  });

  // Set the selectedAddress to the matching address or the current address if no match is found
  selectedAddress.value = matchingAddress || currentAddress;
  showDialog.value = true;
};

/**
 * Updates the selected address in the database and updates the row.
 */
const updateAddress = async () => {
  try {
    const updatedMember = {
      target_id: row.target_id,
      ...selectedAddress.value,
      state: selectedAddress.value.state,
      collection_id: props.collection_id,
    };
    await awsGateway.post('/collection/update-member-contact/', updatedMember);

    const memberIndex = collection.members.findIndex(member => member.member_id === row.member_id);
    if (memberIndex !== -1) {
      collection.members[memberIndex] = {
        ...collection.members[memberIndex],
        address: updatedMember.address,
        city: updatedMember.city,
        state: updatedMember.state,
        postcode: updatedMember.postcode
      };
    }

    Object.assign(row, updatedMember);
    selectedAddress.value = { ...updatedMember };
    showDialog.value = false;
  } catch (error) {
    alert('Failed to update address');
  }
};

/**
 * Handles the confirmation action for updating the address.
 */
const handleConfirm = async () => {
  if (selectedAddress.value) {
    await updateAddress();
  } else {
    alert('No address selected');
  }
};
</script>

<template>
  <div>
    <a href="#" @click.prevent="openDialog">
      <span v-if="row.address">{{ row.address }}<br/></span>
      <span v-if="row.city">{{ row.city }},</span> {{ row.state }} {{ row.postcode }}
    </a>

    <q-dialog v-model="showDialog">
      <q-card style="min-width: 320px;">
        <q-card-section>
          <div class="text-h6">Select an Address</div>
        </q-card-section>

        <q-card-section class="q-pa-none">
          <q-list class="q-pl-sm">
            <q-item class="q-py-none" v-for="(address, index) in addresses" :key="index">
              <q-item-section class="q-py-none q-pl-none justify-left item-section">
                  <q-radio class="q-py-sm" v-model="selectedAddress" :val="address">
                      {{ address.address }}<br>{{ address.city }}, {{ address.state }} {{ address.postcode }}
                  </q-radio>
                  <span class="q-badge text-bold q-ml-sm">{{ address.source }}</span>
              </q-item-section>
            </q-item>
          </q-list>
        </q-card-section>

        <q-card-actions align="right">
          <q-btn color="primary" flat label="Cancel" @click="showDialog = false"/>
          <q-btn color="primary" flat label="OK" @click="handleConfirm"/>
        </q-card-actions>
      </q-card>
    </q-dialog>
  </div>
</template>

<style scoped>
.justify-left {
  justify-content: flex-start;
}
.item-section {
  display: flex;
  align-items: center;
  flex-direction: row;
}
.item-section > .q-badge {
  transition: opacity .3s ease;
  opacity: .5
}
.item-section:hover > .q-badge,
.item-section > .q-checkbox[aria-checked="true"] + .q-badge {
  opacity: 1
}
</style>
