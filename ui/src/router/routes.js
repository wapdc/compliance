
const routes = [
  {
    path: '/',
    component: () => import('@wapdc/quasar-app-extension-pdc-ui/src/components/PUserLayout.vue'),
    children: [
      { path:'', component: () => import('pages/CollectionList.vue'), name: 'CollectionList'},
      { path:'collections/:target', name: 'TargetCollections', component: () => import('pages/CollectionList.vue')},
      { path:'collection/:collection_id', component: () => import('pages/MembersList.vue'), name: 'MembersList'},
      { path:'reports/C4-report-compliance', component: () => import('pages/C4ReportCompliance.vue')},
      { path:'reporting/periods/edit/:year/:type', component: () => import('pages/ReportingPeriodsManagement.vue'), name: 'ReportingPeriodRedirection'},
      { path:'reporting/periods/edit/:type', component: () => import('pages/ReportingPeriodsManagement.vue'), name: 'ReportingPeriodList'},
      { path:'reporting/periods/draft/:year/:type', component: () => import('pages/ReportingPeriodsGenerator.vue'), name: 'ReportingPeriodGenerator'},
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
