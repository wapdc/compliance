import {useGateway} from "@wapdc/pdc-ui/application";
import { reactive, ref } from "vue";
import moment from "moment";
import {useCompliance} from "src/compliance";

// rows for the component to use in the table
export let l2 = reactive([])
export let l3 = reactive([])
export let c4 = reactive([])

/**
 * set the default year to the current year when the page initializes
 * when loading L2, L3 and C4 into reactive objects.
 */
export const selectedYear = ref('')
selectedYear.value = new Date().getFullYear();

/**
 * get all the report periods for a selected year and assign them to the reactive objects
 * of c4, l2 and l3.
 * @returns {Promise<void>}
 */
export async function loadReportPeriodData() {
  const {awsGateway} = await useCompliance()
  if (selectedYear.value) {
    const data = await awsGateway.get(`/report/periods/${selectedYear.value}/all`)
    if(data.length > 0) {
      Object.assign(c4,data.filter((row) => row.report_type === 'C4'))
      Object.assign(l2,data.filter((row) => row.report_type === 'L2'))
      Object.assign(l3,data.filter((row) => row.report_type === 'L3'))
    }
  }
}

// reset reactive variable
export async function resetTable() {
    l2.length = 0;
    l3.length = 0;
    c4.length = 0;
}

/**
 * Update report period
 * @param period_data
 * @returns {Promise<void>}
 */
export async function editReportPeriod(period_data) {
  const reporting_period_id = period_data.reporting_period_id
  const formattedData = {
    "reporting_period_id": period_data.reporting_period_id,
    "reporting_period_type": period_data.reporting_period_type,
    "start_date": moment(period_data.start_date).format('YYYY-MM-DD'),
    "end_date": moment(period_data.end_date).format('YYYY-MM-DD'),
    "due_date": moment(period_data.due_date).format('YYYY-MM-DD'),
    "election_date": period_data.election_date ? moment(period_data.election_date).format('YYYY-MM-DD') : null,
    "report_type": period_data.report_type,
    "reminder_date": period_data.reminder_date ? moment(period_data.reminder_date).format('YYYY-MM-DD') : null
  }

  const {awsGateway} = await useCompliance()
  await awsGateway.put(`/report/periods/${reporting_period_id}/update`, {"period": formattedData}).then(data => {
    c4.value = data.filter((row) => row.report_type === 'C4')
    l2.value = data.filter((row) => row.report_type === 'L2')
    l3.value = data.filter((row) => row.report_type === 'L3')
  })
}


