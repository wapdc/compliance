import {useCompliance} from "src/compliance";
import {reactive, ref} from "vue";

/**
 * set the default year to the current year when the page initializes
 * when loading L2, L3 and C4 into reactive objects.
 */
export const selectedYear = ref('')
selectedYear.value = new Date().getFullYear();

/** Load max year for L2, L3 */
export const startYears = reactive([])

/**
 * Load max year in table for L2, L3
 * @returns {Promise<void>}
 */
export async function loadMaxYear() {
  const {awsGateway} = await useCompliance()
  await awsGateway.get(`/report/periods/all/years`).then (data => {
    Object.assign(startYears, data)
  })
}

export let l2_l3_dates = ref([])

/**
 * Helper function takes a string in the format YYYY-MM-DD converts to a date.
 *   determines the end date of the month, returns the end of the month as a string
 *   in YYYY-MM-DD format.
 *
 * @param dateStr
 * @returns {Promise<string>}
 */
export async function changeIfLeapYear(dateStr) {
  let date = new Date(dateStr);
  date.setMonth(date.getMonth() + 1);
  date.setDate(0);
  return date.getFullYear() + '-' + (date.getMonth() + 1).toString().padStart(2, '0') + '-'
    + date.getDate().toString().padStart(2, '0')
}


/**
 * Generate report periods
 * @param start_year
 * @param end_year
 * @param report_type
 * @returns {Promise<void>}
 */
export async function generateReportPeriods(start_year, end_year, report_type) {
  end_year = parseInt(end_year)

  const {awsGateway} = await useCompliance()
  await awsGateway.get(`/report/periods/generator/${start_year}/${end_year}/${report_type}`).then(data => {
    l2_l3_dates.value = data
  })
}

/**
 * Election data generated from the c4 generator api for creating C4 dates.
 * This data is used to populate the RDS Election's table.
 */
export let c4_election_dates = reactive([])
export let period_dates = reactive([])

/**
 * Generate C4 report periods
 * @param c4_dates
 * @returns {Promise<void>}
 */
export async function createC4ElectionDates(c4_dates) {

  const {awsGateway} = await useCompliance()
  await awsGateway.post(`/report/periods/generator/c4`,
    {
      "february_date": c4_dates.february_date,
      "april_date": c4_dates.april_date,
      "primary_date": c4_dates.primary_date,
      "general_date": c4_dates.general_date
    }).then(data => {
    const {elections, periods} = data
    period_dates = periods;
    c4_election_dates = elections;
  })
}

/**
 * Saves all the rows that are generated from the report period generator
 * @param rows
 * @returns {Promise<void>}
 */
export async function saveElectionDates(rows) {
  const {awsGateway} = await useCompliance()
  await awsGateway.put(`/report/periods/save/dates`, rows.value).then(data => {
    if (data.report_type === 'C4') {
      awsGateway.put('/report/periods/save/election/dates', c4_election_dates.value)
    }
  })
}
