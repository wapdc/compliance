import YearOnly from "./YearOnly.vue";
import collectionBase from "./collectionBase";

export default {
  editor: () => YearOnly,
  selectedColumns: [...collectionBase.selectedColumns, "filer_id"],
  potentialColumns: [
    ...collectionBase.potentialColumns,
    { name: 'filer_id', align: 'left', label: 'Filer Id', field:'filer_id', priority: 2, sortable: true },
  ]
}
