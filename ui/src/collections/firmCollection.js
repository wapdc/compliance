import collectionBase from "src/collections/collectionBase";
import {formatDate} from "@wapdc/pdc-ui/util";
import CollectionEditor from "src/collections/CollectionEditor.vue";
import MonthlyPeriodPicker from "src/collections/MonthlyPeriodPicker.vue";
import RegisteredLobbyistClients from "src/collections/RegisteredLobbyistClients.vue";

function getEditor(template) {
  switch (template) {
    case 'lobbyist-firms-registered':
      return RegisteredLobbyistClients
    case 'lobbyist-firms-monthly-reports-due-for-period':
      return MonthlyPeriodPicker
    default:
      return CollectionEditor;
  }
}

export default {
  editor: getEditor,
  selectedColumns: [...collectionBase.selectedColumns, 'address', 'updated_at'],
  potentialColumns: [ ...collectionBase.potentialColumns,
    // Column definitions specific to lobbyist firms collection
    { name: "phone", label: "Phone", align: "left", sortable: true , field: "phone"},
    { name: "office", label: "Office", align: "left", field: "office", sortable: true },
    { name: "jurisdiction", label: "Jurisdiction", align: "left", field: "jurisdiction", sortable: true },
    { name: "updated_at", label: "Last Updated", align: "left", sortable: true, field: "updated_at", format: formatDate}
  ]
}
