import collectionBase from "src/collections/collectionBase"
import MissingC4ReportsSummary from "components/MissingC4ReportsSummary.vue"
import C4Status from "src/collections/C4Status.vue"

function metadataCsv(row, csvRow) {
  const reportTypes = ["TwentyOneDayPreGeneral", "SevenDayPreGeneral", "PostGeneral", "PostPrimary", "SevenDayPrePrimary", "TwentyOneDayPrePrimary"]
  const metadata = typeof row.metadata === 'string' ? JSON.parse(row.metadata) : row.metadata
  const reports = metadata?.reports || []
  reportTypes.forEach(type => {
    const report = reports.find(r => r.reporting_period_type === type)
    if (report) {
      if (report.reporting_type === "mini") {
        csvRow[type] = "exempt (mini filer)";
      } else {
        csvRow[type] = report.is_filed ? "filed" : "missing";
      }
    }
  })
}
export default {
  editor: () => C4Status,
  selectedColumns: [...collectionBase.selectedColumns, 'reports'],
  potentialColumns: [
    ...collectionBase.potentialColumns,
    {
      name: "reports",
      label: "Reports",
      field: "metadata",
      align: "left",
      csv: metadataCsv,
      component: () => MissingC4ReportsSummary,
    }
  ]
}

