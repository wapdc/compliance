import collectionBase from "src/collections/collectionBase";
import {formatDate} from "@wapdc/pdc-ui/util";
import YearOnly from "src/collections/YearOnly.vue";
import CollectionEditor from "src/collections/CollectionEditor.vue";
import RegisteredLobbyistClients from "src/collections/RegisteredLobbyistClients.vue";

function getEditor(template) {
  switch (template) {
    case 'lobbyist-clients-registered':
      return RegisteredLobbyistClients;
    case 'lobbyist-client-l3-reports-due':
      return YearOnly;
    default:
      return CollectionEditor;
  }
}

export default {
  editor: getEditor,
  selectedColumns: [...collectionBase.selectedColumns, 'address', 'updated_at'],
  potentialColumns: [ ...collectionBase.potentialColumns,
    // Column definitions specific to lobbyist client collection
    { name: "phone", label: "Phone", align: "left", sortable: true , field: "phone"},
    { name: "office", label: "Office", align: "left", field: "office", sortable: true },
    { name: "updated_at", label: "Last Updated", align: "left", sortable: true, field: "updated_at", format: formatDate}
  ]
}
