import collectionBase from "src/collections/collectionBase";
import {formatDate} from "@wapdc/pdc-ui/util";
import OfficialsConfirmedSince from "src/collections/OfficialsConfirmedSince.vue";
import CollectionEditor from "src/collections/CollectionEditor.vue";
import JurisdictionSearch from "src/collections/JurisdictionSearch.vue";

function getEditor(template) {
  switch (template) {
    case 'jurisdiction-officials-confirmation':
      return OfficialsConfirmedSince
    case 'jurisdiction-research':
      return JurisdictionSearch
    default:
      return CollectionEditor;
  }
}

const definition = {
  editor: getEditor,
  selectedColumns: [...collectionBase.selectedColumns, 'jurisdiction','officials_confirmed_at','rpts','ols_updated_at'],
  potentialColumns: [
    ...collectionBase.potentialColumns,
    { name: "jurisdiction", label: "Jurisdiction", align: "left", sortable: true , field: "jurisdiction", },
    { name: "officials_confirmed_at", label: "Officials List Updated", align: "left", field: "officials_confirmed_at", sortable: true, format: formatDate},
    { name: "rpts", label: "Reporting", align: "left", field: "rpts", sortable: true },
    { name: "ols_updated_at", label: "Last Verified", align: "left", field: "ols_updated_at", sortable: true, format: formatDate },
  ]
}
// Exclude cases generated because we don't do group enforcement on jurisdicions.
const genField = definition.potentialColumns.findIndex(c => c.name==='case_generated')
if (genField >=0) definition.potentialColumns.splice(genField, 1)

export default definition

