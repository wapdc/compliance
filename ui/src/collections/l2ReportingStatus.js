import collectionBase from "src/collections/collectionBase";
import MonthlyL2Reports from "components/MonthlyL2Reports.vue";
import L2Status from "src/collections/L2Status.vue";
import moment from "moment";


function metadataCsv(row, csvRow, colDef, collection) {


  const months = moment.monthsShort()
    .map((m, i) => ({label: m, value: i + 1}))
    .filter((m) => (m.value >= collection.metadata.start_month) && (m.value <= collection.metadata.end_month) )

  months.forEach(month => {
      const report = row.metadata.reports.find(r => r.m === month.value)
      csvRow[month.label] = report ? report.status  : ""
  })
}

export default {
  editor: () => L2Status,
  selectedColumns: [...collectionBase.selectedColumns,'case_generated', 'reports'],
  // Here we override the list of potential columns including the ones from the base component.
  potentialColumns: [
    ...collectionBase.potentialColumns,
    // Column definitions specific to a lobbyist collection
    {
      name: "reports",
      label: "Reports",
      field: "metadata",
      align: "left",
      csv: metadataCsv,
      component: () => MonthlyL2Reports,
    }
]
}
