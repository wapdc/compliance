import L3Status from "src/collections/L3Status.vue";
import collectionBase from "src/collections/collectionBase";

export default {
  editor: () => L3Status,
  selectedColumns: [...collectionBase.selectedColumns, 'case_generated','phone'],
  // Here we override the list of potential columns including the ones from the base component.
  potentialColumns: [...collectionBase.potentialColumns,
    // Column definitions specific to a lobbyist collection
    {
      name: "phone",
      label: "Phone",
      field: "phone",
      align: "left"
    }
  ]
}
