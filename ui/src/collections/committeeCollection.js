import collectionBase from "src/collections/collectionBase";
import CommitteeMissingC4Reports from "src/collections/CommitteeMissingC4Reports.vue";
import ElectionCodeOnly from "src/collections/ElectionCodeOnly.vue";

function getEditor (template) {
  switch (template) {
    case "committee-missing-c4-reports":
      return CommitteeMissingC4Reports
    default:
      return ElectionCodeOnly
  }
}

export default {
  editor: getEditor,
  selectedColumns: [...collectionBase.selectedColumns],
  potentialColumns: [...collectionBase.potentialColumns],
}
