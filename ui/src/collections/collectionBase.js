import {formatDate} from "@wapdc/pdc-ui/util";
import SelectContactAddress from "components/SelectContactAddress.vue";
import SelectContactEmail from "components/SelectContactEmail.vue";
import NameLink from "components/NameLink.vue";
import YearOnly from "src/collections/YearOnly.vue";

/**
 * Makes sure csv file contains all address fields even though these
 * are displayed in one column in the datatable.
 * @param source
 * @param destination
 */
function setAddressFields(source, destination) {
  destination.Address = source.address
  destination.City = source.city
  destination.State = source.state
  // Zip is the first 5 characters of the postcode, return empty string if postcode is null or undefined
  destination.Zip = source.postcode?.substring(0, 5) ?? ''
}

/**
 * This is a common library that is meant to house the basic promprs
 */
export default {
  editor: () => YearOnly,
  // This returns a custom component that will facilitate entry of the year as the primary parameter to the collection
  // when implementing custom components you need ot make sure you use CollectionEditor and place your extra fields in
  // the default slot.
  // Determines the default list of selected columns
  selectedColumns: ['name', 'options', 'email', 'contacted', 'memo'],
  // List of available columns.
  //   Columns are sorted by priority, the default priority is 5.
  potentialColumns: [
    { name: 'name',  align: 'left', label: 'Name', sortable: true , field: 'name', priority: 1, component: () => NameLink},
    { name: 'options',  align: 'left', label: 'Options', some_field: 'some value', priority: 1},
    { name: 'status', align: 'left', label: 'Status', field:'flag', priority: 2, sortable: true },
    { name: 'email', align: 'left', label: 'Email', sortable: true , field: 'email',  component: () => SelectContactEmail, priority: 3},
    { name: 'contacted',  align: 'left', label: 'Contacted', sortable: true , field: 'last_contacted', format: formatDate, priority:  3},
    { name: 'memo', align: 'left', label: 'Memo', field: 'memo', sortable: true},
    // Here we override the display of the address so that it includes all address fields in a single column
    // the csv function referenced ensures that CSV exports all fields.
    { name: "address", label: "Address", align: "left", field: "address", sortable: true, component: () => SelectContactAddress, csv: setAddressFields },
    { name: "case_generated", label: "Case Generated", align: "left", field: "compare_id", sortable: true, priority: 3 }
  ]
}
