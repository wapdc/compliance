import collectionBase from "src/collections/collectionBase";
import OfficialsYearJurisdictionCategory from "src/collections/OfficialsYearJurisdictionCategory.vue";

export default {
  editor: () => OfficialsYearJurisdictionCategory,
  selectedColumns: [...collectionBase.selectedColumns, 'status','office','jurisdiction','rpts'],
  potentialColumns: [
    ...collectionBase.potentialColumns,
    // Column definitions specific to a official collection
    { name: "jurisdiction", label: "Jurisdiction", align: "left", sortable: true , field: "jurisdiction"},
    { name: "office", label: "Office", align: "left", field: "office", sortable: true },
    { name: "rpts", label: "Reporting", align: "left", field: "rpts", sortable: true },
    { name: "end_date", label: "End Date", align: "left", field: "end_date", sortable: true },
  ]
}
