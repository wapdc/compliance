import collectionBase from "src/collections/collectionBase";
import ElectionCodeOnly from "src/collections/ElectionCodeOnly.vue";
import CandidateAdvancedSearch from "src/collections/CandidateAdvancedSearch.vue";
import CandidateMissingC4Report from "src/collections/CandidateMissingC4Report.vue";
import {formatDate} from "@wapdc/pdc-ui/util";

function getEditor(template) {
  switch(template) {
    case 'candidacy-advanced-search':
      return CandidateAdvancedSearch
    case 'candidacy-missing-C4-reports':
      return CandidateMissingC4Report
    case 'candidacy-missing-C4-reports-primary':
      return CandidateMissingC4Report
    default:
      return ElectionCodeOnly
  }

}
export default {
  editor: getEditor,
  selectedColumns: [...collectionBase.selectedColumns, 'status','office','jurisdiction','rpts','exit_reason','election_code','primary_result','general_result','declared'],
  // Here we override the list of potential columns including the ones from the base component.
  potentialColumns: [
    ...collectionBase.potentialColumns,
    // Column definitions specific to a candidate collection
    { name: "phone", label: "Phone", align: "left", sortable: true, field: "phone"},
    { name: "office", label: "Office", align: "left", field: "office", sortable: true },
    { name: "jurisdiction", label: "Jurisdiction", align: "left", field: "jurisdiction", sortable: true },
    { name: "rpts", label: "Reporting", align: "left", field: "rpts", sortable: true },
    { name: "exit_reason", label: "Exit Code", align: "left", field: "exit_reason", sortable: true },
    { name: "election_code", label: "Election Code", align: "left", field: "election_code", sortable: true },
    { name: "general_result", label: "General result", align: "left", field: "general_result", sortable: true },
    { name: "primary_result", label: "Primary result", align: "left", field: "primary_result", sortable: true },
    { name: "filer_id", label: "Filer id", align: "left", field: "filer_id", sortable: true },
    { name: "declared", label: "Declared", align: "left", field: "declared", sortable: true },
    { name: "campaign_start_date", label: "Campaign Start Date", align: "left", field: "campaign_start_date", sortable: true, format: formatDate }
  ]
}
