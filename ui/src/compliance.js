import Papa from "papaparse";
import {reactive} from "vue";
import {useApollo, useGateway} from "@wapdc/pdc-ui/application";
import collectionBase from "src/collections/collectionBase";
import candidateCollection from "src/collections/candidateCollection";
import officialCollection from "src/collections/officialCollection";
import jurisdictionCollection from "src/collections/jurisdictionCollection";
import firmCollection from "src/collections/firmCollection";
import personCollection from "src/collections/personCollection";
import L2ReportingStatus from "src/collections/l2ReportingStatus";
import clientCollection from "src/collections/clientCollection";
import L3ReportingStatus from "src/collections/l3ReportingStatus";
import committeeCollection from "src/collections/committeeCollection";
import C4ReportingStatus from "src/collections/C4ReportingStatus";
/**
 * Generates a CSV file from a set of rows and columns
 * @param filename
 * @param rows
 * @param columns
 * @param collection
 *   Collection for purposes of getting at metadata.
 */
export function generateCSV(filename, rows, columns, collection) {
  // necessary because of vue Reactivity
  rows = JSON.parse(JSON.stringify(rows))

  const csvData = rows.map(row => {
    let r = {}
    columns.forEach(c => {
      let val = row[c.field]
      // If no field is defined, it is for visual controls only, so don't include in the csv in row.
      if (c.csv) {
        c.csv(row, r, c, collection)
      }
      else if (c.field) {
        // Run custom formatter if defined.
        if (c.format) {
          val = c.format(val)
        }
        r[c.label] = val
      }
    })
    return r
  })

  // prepare csv
  const csv = Papa.unparse(csvData);
  const csvBlob = new Blob([csv], {type: 'text/csv;charset=utf-8;'});

  if (navigator.msSaveBlob) {
    navigator.msSaveBlob(csvBlob, filename);
  } else {
    const url = URL.createObjectURL(csvBlob);
    const link = document.createElement('a');
    link.href = url;
    link.download = filename;
    link.click();
    URL.revokeObjectURL(url);
  }
}

/**
 * Compliance case manipulation
 */
const caseLookup = reactive({})

export async function useCaseTracker () {
  const {awsGateway} = await useCompliance();
  const caseTrackerService = useApollo('compliance/services')
  if (!caseLookup.political_categories) {
    const data = await awsGateway.get('case-lookup')
    Object.assign(caseLookup, data)
  }
  return {caseLookup, awsGateway, caseTrackerService}
}

/**
 * Definition of candidacy collections according to the
 */
const collectionTargets =
  // list of collections relevant to candidacies
  [
    {
       label: "Candidates",
       target: 'candidacy',
       ui: candidateCollection,
       collections: [
         { label: 'Candidates missing FA statements and registration', template: 'candidates-missing-statements-and-registrations' },
// The following two types are commented out because we can't really do publishing collections yet as they are excluded from
// the list of collections.  This is because these types of collections are primarily used for assigning filer id's but
// that code is not currently ported from the core project.
//       { label: 'Candidates with election results', template: 'candidacy-with-election-results' },
//         { label: 'Candidates missing filer ids', template: 'candidacy-missing-filer-id' },
         { label: 'Candidates missing positions', template: 'candidacy-missing-positions' },
         { label: 'Candidates with advanced search', template: 'candidacy-advanced-search'},
         { label: 'Candidates missing C4 reports  - General', template: 'candidacy-missing-C4-reports'},
         { label: 'Candidates missing C4 reports - Primary', template: 'candidacy-missing-C4-reports-primary'},
         { label: 'Candidates missing C4 reports Summary - General', template: 'candidacy-missing-C4-reporting-summary', ui: C4ReportingStatus},
         { label: 'Candidates missing C4 reports Summary - Primary', template: 'candidacy-missing-C4-reporting-summary-primary', ui: C4ReportingStatus}
       ]
    },
    {
      label: 'Committees',
      target: 'committee',
      ui: committeeCollection,
      collections: [
        {
          label: 'Committees missing C4 reports',
          template: 'committee-missing-c4-reports'},
      ]
    },
    {
      label: "Jurisdiction",
      target: 'jurisdiction',
      ui: jurisdictionCollection,
      collections: [
        {label: 'All jurisdictions', template: 'jurisdiction-research'},
        {label: 'Jurisdiction un-confirmed since date', template: 'jurisdiction-officials-confirmation'}
      ]
    },
    {
      label: 'Lobbyist clients',
      target: 'lobbyist_client',
      ui: clientCollection,
      collections: [
        { label: 'Registered lobbyist clients', template: 'lobbyist-clients-registered'},
        { label: 'L3 compliance', template: 'lobbyist-client-l3-reporting', ui: L3ReportingStatus}
      ]
    },
    {
      label: "Lobbyist firms",
      target: 'lobbyist_firm',
      ui: firmCollection,
      collections: [
        { label: 'Registered lobbyist firms', template: 'lobbyist-firms-registered'},
        { label: 'L2 compliance', template: 'lobbyist-firms-l2-reporting', ui: L2ReportingStatus},
        { label: 'Monthly lobbyist report due', template: 'lobbyist-firms-monthly-reports-due-for-period'},
      ]
    },
    {
      label: 'Officials',
      target: 'officials',
      ui: officialCollection,
      collections: [
        {label: 'Officials missing fa statements', template: 'officials-missing-fa-statement'},
      ]
    },
    {
      label: 'People',
      target: 'person',
      ui: personCollection,
      collections: [
        {label: 'People indicating a reporting modification', template: 'person-collection-reporting-modification'},
      ]
    },
  ]

// Ensure collectionTargets is always alphabetically sorted
collectionTargets.sort((a, b) => a.label.localeCompare(b.label));

/**
 * Retrieves the user interface object for a collection that
 * @param target
 * @param template
 * @return Object Collection user interface definition (see collections folder for possibles)
 */
function getCollectionUi(target, template) {

  const targetDefinition = collectionTargets.find(c => c.target === target)
  if (targetDefinition) {
    const collectionDef = targetDefinition.collections.find(c => c.template === template.replace('.yml',''))
    if (collectionDef && collectionDef.ui) {
      return collectionDef.ui
    }
    else if (targetDefinition.ui) {
      return targetDefinition.ui
    }
  }
  return collectionBase
}

export function useCollections() {
  return {collectionTargets, getCollectionUi}
}

export function formatC4ReportType(type) {
  const mappings = {
    "TwentyOneDayPreGeneral": "21-day pre-general",
    "PostGeneral": "post-general",
    "TwentyOneDayPreElection": "21-day pre-election",
    "SevenDayPreGeneral": "7-day pre-general",
    "PostPrimary": "post-primary",
    "TwentyOneDayPrePrimary": "21-day pre-primary",
    "SevenDayPrePrimary": "7-day pre-primary",
    "SevenDayPreElection": "7-day pre-election",
    "PostElection": "post-election"
  }
  return mappings[type] || type
}
/**
 * General library for
 */
export async function useCompliance() {

  const awsGateway = await useGateway('compliance_aws_gateway', 3030);
  return {awsGateway}
}

