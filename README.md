# Compliance

This project provides research and outreach collections for the purpose of helping filers stay in compliance with the 
reporting requirements. 

## Defining a new collection

To define a new research collection you need to modify both front end and back end code in the following ways: 

- Create a definition .yml file in  [aws/collections](aws/collections)
- Add the collection to the appropriate collections array of collectionTargets in [ui/src/compliance.js](ui/src/compliance.js)
- If you need custom ui components for data entry new user interface module in [ui/src/collections](ui/src/collections)

You can refer to comments in the collection definition files for additional information regarding the collections. 

## Testing daily tasks. 

To test the daily tasks function from the aws subfolder execute: 

```shell
npm run daily-tasks
```