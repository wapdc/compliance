#!/usr/bin/env bash
export PGHOST=127.0.0.1
export PGDATABASE=wapdc
export PGPASSWORD=wapdc
export PGUSER=wapdc
export PGPORT=5444
export STAGE=dev
export PSQL_CMD="psql --single-transaction -v ON_ERROR_STOP=1"
npm run --prefix aws build:rds
